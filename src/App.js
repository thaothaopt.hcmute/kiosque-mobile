import React, { Component } from 'react';
import {
  StyleSheet
} from 'react-native';
import { Provider } from 'react-redux';
import { YellowBox } from 'react-native';
import {RootNavigator} from './navigators/RootNavigator';
import configureStore from './utils/store';

const store = configureStore();
class App extends Component {
  constructor(props) {
    super(props);
    // disable warning react-navigation
    YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader']);
  }
  render() {
    return (
      <Provider store={store}>
        <RootNavigator />
      </Provider >
    );
  }
}

const styles = StyleSheet.create({
});
export default App;
