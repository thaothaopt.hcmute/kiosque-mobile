import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View
} from 'react-native';
import TextBox from '../../components/TextBox';
import Dash from '../../components/Dash';
import Colors from '../../contants/colors';
import Screen from '../../utils/screen';

class InputInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
      const { onChangeText, placeholder, title } = this.props;
    return (
      <View style={styles.container}>
        <Text style={styles.text}>
          {title}
        </Text>
        <View style={{ width: '100%'}}>
        <TextBox
         // icon="phone"
          placeholder={placeholder}
          onChangeText={onChangeText}
          selectionColor='#d4cfcf'
          placeholderTextColor={Colors.DISABLE_COLOR}
          styleTextBox={{color:'#ffffff'}}
        />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
   marginVertical: 10,
    paddingRight: 20,
    paddingLeft: 20,
    width: 300
  },
  text: {
    fontSize: Screen.width(3.5),
    color: '#d4cfcf',
    justifyContent: "center",
    alignItems: "center",
  }
});
export default InputInfo;