import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity
} from 'react-native';
import Colors from '../../contants/colors';
import Screen from '../../utils/screen';

class Pledge extends Component {

  render() {
    const { onPressRule } = this.props;
    return (
      <View style={styles.container}>
        <Text style={{ fontSize: Screen.width(3), color: Colors.TEXT_COLOR}}>
          {'We don\'t use your info to any other goal.'}
        </Text>
        <TouchableOpacity onPress={onPressRule}>
          <Text style={{ fontSize: Screen.width(3), color: Colors.PRIMARY_COLOR}}>
          {'Kiosque\'s rule.'}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: Screen.height(25)
  }
});
export default Pledge;