import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  Image,
  Dimensions,
  View,
  WebView,
  ImageBackground,
  Dimension,
  TouchableOpacity
} from "react-native";
import { connect } from "react-redux";
import Colors from "../../contants/colors";
import Strings from "../../contants/strings";
import Container from "../../components/Container";
import InputInfo from "./InputInfo";
import Pledge from "./Pledge";
import Logo from "../../components/Logo";
import Button from "../../components/Button";
import Screen from "../../utils/screen";
import Skip from "./Skip";
import { requsetLogin } from "../../redux/actions/loginActions";
import { requestGetUserInfo } from "../../redux/actions/getUserInfoActions";
import colors from "../../contants/colors";
class LogInScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      phoneNo: "",
      clickedLogin: false
    };
  }

  componentDidMount() {
    const { login, dispatchGetUserInfo, navigation } = this.props;
    if (login.logged) {
      dispatchGetUserInfo(login.auth._id);
      navigation.navigate("TabBarNavigator");
    }
  }

  componentWillReceiveProps(nextProps) {
    const { navigation } = this.props;
    const { login } = nextProps;
    const { clickedLogin } = this.state;
    if (login.isFetching && clickedLogin) {
    } else if (login.logged && clickedLogin) {
      this.setState({ clickedLogin: false }, () => {
        navigation.navigate("TabBarNavigator");
      });
    }
  }

  onPressLogIn = phoneNo => {
    const { dispatchLogin } = this.props;
    if (!phoneNo) {
      this._.showMessage("", Strings.NEWPHONE, "Ok", () => { });
    } else if (phoneNo.match(/[^0-9]/) || phoneNo.length != 10) {
      this._.showMessage("", Strings.INVALIDPHONE, "Ok", () => { });
    } else {
      this.setState({ clickedLogin: true }, () => {
        dispatchLogin({ phone: phoneNo });
      });
    }
  };

  onPressSkip = () => {
    const { navigation } = this.props;
    navigation.navigate("TabBarNavigator");
  };

  onPressRule = () => { };

  render() {
    const { navigation } = this.props;
    const { phoneNo } = this.state;
    return (
      <Container scrollable ref={component => (this._ = component)}>

        <ImageBackground
          source={require('../../assets/images/bg2.jpg')}
          style={styles.backgroundImage}
          resizeMode='stretch' // or 'stretch'
        >
          {/* <View style={styles.container}> */}
          {/* <View style={{paddingHorizontal:5, height: 90 }}/> */}
          {/* <Logo /> */}

          <View style={styles.area}>
            <Text style={styles.login}>LOGIN</Text>

            <InputInfo
              placeholder={"Phone number"}
              onChangeText={text => this.setState({ phoneNo: text })}
            />
            <TouchableOpacity
              style={{ backgroundColor: "#e4fde9", width: '70%', alignItems: 'center', justifyContent: 'center' }}
              onPress={() => {
                // this.onPressLogIn(phoneNo);
                navigation.navigate('VerifyScreen')
              }}
              
            >
              <Text style={styles.textbtn}> CONTINUE </Text>
            </TouchableOpacity>
            <Skip onPress={() => this.onPressSkip()} />
            {/* <Pledge onPressRule={() => this.onPressRule()} /> */}

          </View>
          {/* </View> */}
        </ImageBackground>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    //backgroundImage: '../../assets/images/bg.png',
    marginHorizontal: 15,
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    // backgroundColor: 'rgba(52, 52, 52, 0.5)',
    // height : Dimensions.get('window').height/5,
  },
  area: {
    marginHorizontal: 40,
    height: 100,
    width: 300,
    backgroundColor: 'rgba(10,10,10, 0.8)',
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 150,
    marginTop: 150,
    borderRadius: 20
  },
  logoView: {
    justifyContent: "center",
    alignItems: "center",
    flex: 1
  },
  login: {
    marginTop:20,
    //  alignItems: "center",
    flex: 1,
    fontSize: 40,
    color: "white",
    letterSpacing: 5,
      },
  textbtn: {
    letterSpacing: 1,
    color: Colors.PRIMARY_COLOR,
    padding: 10,
    fontWeight: 'bold',
    marginLeft: 25,
    marginRight:25,
  },
  backgroundImage: {
    flex: 1,

    height: Dimensions.get('window').height
  }
});
const mapStateToProps = state => ({
  login: state.login,
  getUserInfo: state.getUserInfo
});

const mapDispatchToProps = dispatch => ({
  dispatchLogin: data => dispatch(requsetLogin(data)),
  dispatchGetUserInfo: data => dispatch(requestGetUserInfo(data))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LogInScreen);
