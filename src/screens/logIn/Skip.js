import React, { Component } from 'react';
import {
  StyleSheet, TouchableOpacity,
  Text,
  View
} from 'react-native';
import Colors from '../../contants/colors';
import Screen from '../../utils/screen';

class Skip extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }


  render() {
      const { onPress } = this.props;
    return (
      <View style={styles.container}>
      <TouchableOpacity
      onPress={onPress}
      >
        <Text style={[styles.welcome, {fontWeight: 'bold'}]}>
          {'Skip'}
        </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  welcome: {
    fontSize: Screen.width(4),
    textAlign: 'center',
    margin: 10,
    color: Colors.PRIMARY_COLOR
  },
  instructions: {
    textAlign: 'center',
    color: Colors.PRIMARY_COLOR,
    marginBottom: 5,
  },
});
export default Skip;