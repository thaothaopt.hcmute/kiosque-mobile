import React, { Component } from "react";
import { StyleSheet, Text, View } from "react-native";
//import { GoogleMap, Marker } from "react-google-maps";
import MapView from "react-native-maps";
class StoresMainScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      region: {
        latitude: 10.808019,
        longitude: 106.688746,
        latitudeDelta: 0.01,
        longitudeDelta: 0.01
      }
    };
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <MapView style={{ flex: 1 }} region={this.state.region}>
          <MapView.Marker
            coordinate={this.state.region}
            title="K Coffee"
            desciption={"120A Hoàng Hoa Thám"}
          />
        </MapView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  }
});
export default StoresMainScreen;
