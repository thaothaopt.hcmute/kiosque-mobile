import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View
} from 'react-native';
import Colors from '../../../contants/colors';
import Dash from '../../../components/Dash';
import ItemOptionH from '../../../components/ItemOptionH';
import ItemOptionV from '../../../components/ItemOptionV';
import Screen from '../../../utils/screen';
import Strings from '../../../contants/strings';

class Board extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }


  render() {
    const { onPressDerection, onPressHistory, reward } = this.props;
    return (
      <View>
      <View style={{
        flexDirection: 'row',
        padding: Screen.width(4),
        justifyContent: 'space-between',
        margin: Screen.width(4)
        }}>
        <ItemOptionV
          title="Useful money"
          icon="star"
          disabled
          value={(reward > 0) ?`${reward} vnd` : 'Order more to get reward'}
        />
        <ItemOptionV
          title={'Coupon'}
          icon="loyalty"
          disabled
          value={'DISCOUNTCODE'}
        />
      </View>
        <Dash />
        <View style={{ flexDirection: 'column' }}>
        <ItemOptionH
          onPress={onPressHistory}
          title="History get rewads?"
          icon="history"
          iconRight="chevron-right"
        />
        <ItemOptionH
          onPress={onPressDerection}
          title="How to get rewards?"
          icon="lightbulb-outline"
          iconRight="chevron-right"
        />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    margin: Screen.width(4),
    borderRadius: 3,
    borderColor: Colors.PRIMARY_COLOR
  }
});
export default Board;