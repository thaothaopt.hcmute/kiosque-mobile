import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View
} from 'react-native';
import { connect } from 'react-redux';
import Container from '../../../components/Container';
import ToolBar from '../../../components/ToolBar';
import Avatar from './Avatar';
import Board from './Board';
import Strings from '../../../contants/strings';
import { requestGetReward } from '../../../redux/actions/getRewardActions';

class RewardsScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  componentDidMount() {
    const { dispatchRequestGetReward, login } = this.props;
    dispatchRequestGetReward(login.auth._id);
  }

  onPressDerection = () => {

  }

  onPressHistory = () => {
    
  }


  render() {
    const { navigation, getUserInfo, getReward } = this.props;
    return (
      <Container
      loading={getReward.isFetching}
      toolbar={(
        <ToolBar
        iconLeft="arrowleft"
        onPressIconLeft={() => navigation.goBack()}
        parentTitle={Strings._reward}
        />
      )}
        scrollable
        ref={component => this._ = component}
      >
      <Avatar
        username={getUserInfo.userinfo.username}
        userPicture={getUserInfo.userinfo.userImage}
        money={getUserInfo.userinfo.rewards}
      />

      <Board
        reward={getReward.reward}
        onPressDerection={() => this.onPressDerection()}
        onPressHistory={() => this.onPressHistory()}
      />
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  }
});

const mapStateToProps = state => ({
  login: state.login,
  getUserInfo: state.getUserInfo,
  getReward: state.getReward
});
const mapDispatchToProps = dispatch => ({
  dispatchRequestGetReward: (data) => dispatch(requestGetReward(data))
});
export default connect(mapStateToProps, mapDispatchToProps)(RewardsScreen);