import React, { Component } from 'react';
import {
  StyleSheet,
  View, Text
} from 'react-native';
import Entypo from 'react-native-vector-icons/Entypo';
import UserPicture from '../../../components/UserPicture';
import Colors from '../../../contants/colors';
import Strings from '../../../contants/strings';
import Screen from '../../..//utils/screen';
import RewardsText from '../../../components/RewardsText';

class Avatar extends Component {

  render() {
    const { userPicture, username, onPressUserPicture, money } = this.props;
    return (
      <View style={styles.container}
      >
        <UserPicture
          onPressUserPicture={onPressUserPicture}
          style={{ width: Screen.width(20), height: Screen.width(20), borderRadius: Screen.width(10) }}
          userPicture={userPicture}
          disable
        />
        <Text style={{ fontSize: Screen.width(4), marginTop: Screen.width(2), color: Colors.TEXT_COLOR }}>
          {username}
        </Text>
        <RewardsText
          money={money}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
    margin: Screen.width(4),
    backgroundColor: Colors.BACKGROUND_COLOR,
    padding: Screen.width(4)
  }
});
export default Avatar;