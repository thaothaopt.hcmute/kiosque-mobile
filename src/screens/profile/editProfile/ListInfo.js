import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View
} from 'react-native';
import Colors from '../../../contants/colors';
import Dash from '../../../components/Dash';
import TextBox from '../../../components/TextBox';
import Screen from '../../../utils/screen';

class ListInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }


  render() {
    const { username, fullname, phone, onChangeTextusername, onChangeTextfullname, onChangeTextphone} = this.props
    return (
      <View style={{ margin: 10 }}>
        <View style={{ flexDirection: 'column', paddingVertical: 10 }}>
          <Text style={styles.title}>
            {'Username'}
          </Text>
          <TextBox
            placeholder={username}
            onChangeText={onChangeTextusername}
            selectionColor={Colors.PRIMARY_COLOR}
          />
        </View>

        <View style={{ flexDirection: 'column', paddingVertical: 10 }}>
          <Text style={styles.title}>
            {'Fullname'}
          </Text>
          <TextBox
            placeholder={fullname}
            onChangeText={onChangeTextfullname}
            selectionColor={Colors.PRIMARY_COLOR}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  title: {
    color: '#666666',
    marginBottom: -10,
    fontSize: Screen.width(4)
  }
});
export default ListInfo;