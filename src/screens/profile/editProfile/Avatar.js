import React, { Component } from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  View
} from 'react-native';
import Entypo from 'react-native-vector-icons/Entypo';
import EditAvatar from '../../../components/EditAvatar';
import Colors from '../../../contants/colors';

class Avatar extends Component {

  render() {
    const { userPicture, onImagePicked } = this.props;
    return (
      <TouchableOpacity style={styles.container}
      >
        <EditAvatar
          onImagePicked={imageData => onImagePicked && onImagePicked(imageData)}
          style={{ width: 80, height: 80, borderRadius: 40 }}
          userPicture={userPicture}
        />
        <View style={styles.icon}>
        <Entypo
          name="pencil"
          size={20}
          color={Colors.BACKGROUND_COLOR}
        />
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    marginVertical: 15
  },
  icon: {
    width: 24, height: 24,
    borderRadius: 12,
    backgroundColor: Colors.PRIMARY_COLOR,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: -25,
    alignSelf: 'flex-end',
    marginBottom: 5
    }
});
export default Avatar;