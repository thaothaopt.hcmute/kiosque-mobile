import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View
} from 'react-native';
import { connect } from 'react-redux';
import XDate from 'xdate';
import Container from '../../../components/Container';
import PhoneText from '../../../components/PhoneText';
import ListInfo from './ListInfo';
import ToolBar from '../../../components/ToolBar';
import Colors from '../../../contants/colors';
import Avatar from './Avatar';
import strings from '../../../contants/strings';
import { requestGetUserInfo } from '../../../redux/actions/getUserInfoActions';
import { requestUpdateProfile, uploadImage } from '../../../redux/service/api';

class EditProfileScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      enable: false,
      username: '',
      fullname: '',
    };
  }

  onImagePicked = (img) => {
    console.warn('img: ', img);
    const { login } = this.props;
    const name = `avatar-${new XDate().getTime().toString()}.${img.path.split('.')[img.path.split('.').length - 1]}`
    uploadImage({
      userid: login.auth._id,
      pathToImageOnFilesystem: img.path,
      name: name
    }).then((res) => {
      console.warn('res: ', res);
    }).catch((error) => {
      console.warn('error: ', error);
    })
  }

  onPressIconRight = (userinfo, auth) => {
    const { username, fullname } = this.state;
    const { dispatchGetUserInfo } = this.props;
    const dataPatch = {
      userid: userinfo.authid,
      params: [
        {
          key: 'username',
          value: username || userinfo.username,
        },
        {
          key: 'fullname',
          value: fullname || userinfo.fullname,
        }
      ]
    };
    this._.showLoading();
      requestUpdateProfile(dataPatch).then((res) => {
        if (res.data.status) {
          dispatchGetUserInfo(userinfo.authid);
        }
        this._.hideLoading();
      }).catch((error) => {
        console.warn('er: ', error);
      });
  }

  render() {
    const { navigation, getUserInfo, login } = this.props;
    const { username, fullname } = this.state;
    return (
      <Container
      toolbar={(
        <ToolBar
        iconLeft="arrowleft"
        onPressIconLeft={() => navigation.goBack()}
        iconRight={(username || fullname) ? 'save' : ''}
        onPressIconRight={() => this.onPressIconRight(getUserInfo.userinfo, login.auth)}
        title
        parentTitle={strings.PROFILE}
        />
      )}
        scrollable
        ref={component => this._ = component}
      >
        <Avatar
          onImagePicked={(img) => {
            this.onImagePicked(img);
          }}
          userPicture={getUserInfo.userinfo.userImage}
        />
        <ListInfo
          username={getUserInfo.userinfo.username}
          fullname={getUserInfo.userinfo.fullname}
          phone={login.auth.phone}
          onChangeTextusername={(text) => this.setState({ username: text })}
          onChangeTextfullname={(text) => this.setState({ fullname: text })}
        />
        <PhoneText
        onPressPhone={() => navigation.navigate('UpdatePhoneScreen')}
        phone={login.auth.phone}
        />
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

const mapStateToProps = state => ({
  login: state.login,
  getUserInfo: state.getUserInfo
});

const mapDispatchToProps = dispatch => ({
  dispatchGetUserInfo: (data) => dispatch(requestGetUserInfo(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(EditProfileScreen);