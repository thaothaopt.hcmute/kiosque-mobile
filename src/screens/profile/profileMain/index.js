import React, { Component } from 'react';
import {
  StyleSheet, View
} from 'react-native';
import { connect } from 'react-redux';
import Header from '../../../components/Header';
import Container from '../../../components/Container';
import Empty from '../../../components/Empty';
import ItemOptionH from '../../../components/ItemOptionH';
import Colors from '../../../contants/colors';
import Strings from '../../../contants/strings';
import Screen from '../../../utils/screen';
import ModalRating from '../../../components/modalRating';
import { requestPostReview } from '../../../redux/service/api';

const styles = StyleSheet.create({
  break: {
    height: 10,
    width: '100%',
    backgroundColor: Colors.SECOND_COLOR
  },
  uerImg: {
    height: Screen.width(14),
    width: Screen.width(14),
    borderRadius: Screen.width(7)
  }
});
class ProfileMainScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      content: ''
    };
  }

  onPressUserPicture = () => {
    const { navigation, login } = this.props;
    if (login.logged) {
      navigation.navigate('EditProfileScreen');
    } else {
      navigation.navigate('LogInScreen');
    }
  }

  onPressSubmitReview = (text) => {
    const { login } = this.props;
    let ratingData = {
      content: text
    };
    if (login.logged) {
      ratingData = { ...ratingData, authid: login.auth._id };
    }
    this.setModalVisible(false);
    this._.showLoading();
    requestPostReview(ratingData).then((res) => {
      this._.hideLoading();
      this._.showMessage('Send review',
      'Thank for your review!',
      'OK',
      () => {}
      )
    }).catch((error) => {
      console.warn('error: ', error);
    })
  }

  onPressClock = (navigation) => {
    navigation.navigate('HistoryScreen', {
      userid: 1
    });
  }

  onPressMark = (navigation) => {
    navigation.navigate('RewardsScreen');
  }

  onPressRating = () => {
    
  }

  setModalVisible = (e) => {
    this.setState({
      modalVisible: e
    });
  }

  onPressLogOut = (navigation) => {
    // navigation.navigate('LastStepOrderScreen');
  }

  render() {
    const { getUserInfo, login, navigation } = this.props;
    const { modalVisible, content } = this.state;
    return (
      <Container
        toolbar={(
          <Header
            onPressUserPicture={() => this.onPressUserPicture()}
            username={getUserInfo.userinfo.username || Strings.LOGIN}
            money={getUserInfo.userinfo.rewards || ''}
            image={styles.uerImg}
            userPicture={getUserInfo.userinfo.userImage || ''}
          />
        )}
        scrollable
        ref={component => this._ = component}
      >
        <View style={styles.break} />
        {login.logged
          ? (
            <View style={{ flexDirection: 'column', width: '100%' }}>
              <ItemOptionH
                onPress={() => this.onPressClock(navigation)}
                title="History"
                icon="history"
                style={{
                  paddingVertical: Screen.width(2)
                }}
              />

              <ItemOptionH
                onPress={() => this.onPressMark(navigation)}
                title="Rewards"
                icon="stars"
                style={{
                  paddingVertical: Screen.width(2)
                }}
              />

              <ItemOptionH
                onPress={() => this.setModalVisible(true)}
                title="Review"
                icon="rate-review"
                style={{
                  paddingVertical: Screen.width(2)
                }}
              />

              <ItemOptionH
                onPress={() => this.onPressLogOut(navigation)}
                title="Log Out"
                icon="exit-to-app"
                style={{
                  paddingVertical: Screen.width(2)
                }}
              />
            </View>
          )
          : (
            <ItemOptionH
              onPress={() => this.setModalVisible(true)}
              title="Review"
              icon="rate-review"
              style={{
                paddingVertical: Screen.width(2)
              }}
            />
          )}
          <ModalRating
            visible={modalVisible}
            closeModal={() => this.setModalVisible(false)}
            onPressSubmitReview={() => this.onPressSubmitReview(content)}
            onChangeText={(text) => this.setState({ content: text })}
            disabled={content.length < 0 ? true : false}
          />
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  login: state.login,
  getUserInfo: state.getUserInfo
});

const mapDispatchToProps = dispatch => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(ProfileMainScreen);