import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View
} from 'react-native';
import { connect } from 'react-redux';
import { Tab, Tabs } from 'native-base';
import Container from '../../../components/Container';
import ToolBar from '../../../components/ToolBar';
import Strings from '../../../contants/strings';
import Colors from '../../../contants/colors';
import DoneOrdersTab from './DoneOrdersTab';
import AnotherTab from './AnotherTab';
import Screen from '../../../utils/screen';
import { requestGetHistory } from '../../../redux/actions/getHistoryActions';

class HistoryScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [
        {
          _id: 1,
          productname: [
            'Black coffe',
            'Lychee Tea',
            'Capuchino'
          ],
          total: 130000,
          address: 'Linh Trung Ward, Thu Duc District',
          status: 1 // success
        },
        {
          _id: 2,
          productname: [
            'Black coffe',
            'Lychee Tea',
            'Capuchino',
            'Black coffe',
          ],
          total: 150000,
          address: 'Hiep Phu Ward, District 9',
          status: -1 // pending
        },
        {
          _id: 3,
          productname: [
            'Capuchino'
            // 'Black coffe',
          ],
          total: 80000,
          address: 'Linh Trung Ward, Thu Duc District',
          status: 0 //failed
        }
      ]
    };
  }

  componentDidMount() {
    const { dispatchRequestGetHistory, login } = this.props;
    dispatchRequestGetHistory(login.auth._id);
  }

  render() {
    const { navigation, getHistory } = this.props;
    const { data } = this.state;
    return (
      <Container
        loading={getHistory.isFetching}
        toolbar={(
          <ToolBar
            iconLeft="arrowleft"
            onPressIconLeft={() => navigation.goBack()}
            parentTitle={Strings.HISTORY}
          />
        )}
        scrollable
        ref={component => this._ = component}
      >
        {getHistory.isSuccess
          ? (
            <Tabs
              tabBarUnderlineStyle={{ backgroundColor: Colors.BACKGROUND_COLOR, height: 3 }}
            >
              <Tab
                heading={Strings.DONEORDERS}
                tabStyle={styles.tabStyle}
                activeTabStyle={styles.tabStyle}
                textStyle={styles.textStyle}
                activeTextStyle={[{ fontWeight: 'bold' }, styles.textStyle]}
              >
                <DoneOrdersTab
                  listOrders={getHistory.data}
                />
              </Tab>
              <Tab
                heading={Strings.ANOTHER}
                activeTabStyle={styles.tabStyle}
                tabStyle={styles.tabStyle}
                textStyle={styles.textStyle}
                activeTextStyle={[{ fontWeight: 'bold' }, styles.textStyle]}
              >
                <AnotherTab />
              </Tab>
            </Tabs>
          )
          : (<View />)}
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  tabStyle: {
    backgroundColor: Colors.PRIMARY_COLOR
  },
  textStyle: {
    fontSize: Screen.width(4),
    color: Colors.TEXT_TABBAR_TITLE
  },
});
const mapStateToProps = state => ({
  login: state.login,
  getHistory: state.getHistory
});

const mapDispatchToProps = dispatch => ({
  dispatchRequestGetHistory: (data) => dispatch(requestGetHistory(data))
});
export default connect(
  mapStateToProps,
  mapDispatchToProps)(HistoryScreen);