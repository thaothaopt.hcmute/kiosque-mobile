import React, { Component } from 'react';
import {
  StyleSheet,
  Text, FlatList,
  View, TouchableOpacity
} from 'react-native';
import Dash from '../../../components/Dash';
import OrderItem from '../../../components/OrderItem';

class DoneOrdersTab extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  onPressOrder = (item) => {
    console.warn('i: ', item);
  }

  renderProductName = (products, names) => {
    products.forEach((e, index) => {
      names.push(e.product.name);
    });
    
    return names;
  }

  renderNewsEvent = ({ item }) => {
    return (
      <OrderItem
      productname={this.renderProductName(item.orderlist, [])}
      _id={item._id}
      total={item.total}
      status={item.status}
      onPressOrder={() => this.onPressOrder(item)}
      time={item.dateoforder}
      />
    );
  }

  render() {
    const { listOrders } = this.props;
    return (
      <View>
      <FlatList
        data={listOrders}
        renderItem={this.renderNewsEvent}
        keyExtractor={(item, key) => key.toString()}
        extraData={this.state}
        ItemSeparatorComponent={() => <Dash /> }
      />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  }
});
export default DoneOrdersTab;