import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View
} from 'react-native';
import Container from '../../components/Container';
import InputInfo from '../logIn/InputInfo';
import ToolBar from '../../components/ToolBar';
import Strings from '../../contants/strings';
import Colors from '../../contants/colors';
import Screen from '../../utils/screen';
import Button from '../../components/Button';

class UpdatePhoneScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
        phoneNo: ''
    };
  }

  onPressContinue = (phoneNo, navigation) => {
    if (!phoneNo) {
        this._.showMessage('', Strings.NEWPHONE, 'Ok', () => {});
    } else if (phoneNo.match(/[^0-9]/) || phoneNo.length != 10) {
        this._.showMessage('', Strings.INVALIDPHONE, 'Ok', () => {});
    } else {
        navigation.navigate('VerifyScreen');
    }
  }

  render() {
      const { navigation } = this.props;
      const { phoneNo } = this.state;
    return (
        <Container
        toolbar={(
        <ToolBar
        iconLeft="arrowleft"
        onPressIconLeft={() => navigation.goBack()}
        title
        parentTitle={Strings.UPDATEPHONE}
        />
      )}
        scrollable
        ref={component => this._ = component}
      >
      <View style={{ marginHorizontal: 15, justifyContent: 'center' }}>
      <InputInfo
      title={Strings.UPDATEPHONE}
            placeholder={'0xxxx'}
            onChangeText={(text) => this.setState({ phoneNo: text })}
          />
          <Button
            titleBtn={Strings.CONTINUE}
            onPress={() => this.onPressContinue(phoneNo, navigation)}
            colorText={Colors.TEXT_COLOR_2}
            styleBtn={{ width: '100%', height: Screen.width(9) }}
          />
      </View>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
export default UpdatePhoneScreen;