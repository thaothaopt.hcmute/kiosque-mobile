import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View
} from 'react-native';
import Container from '../../components/Container';
import ToolBar from '../../components/ToolBar';
import Colors from '../../contants/colors';
import Screen from '../../utils/screen';


class SubCartScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }


  render() {
    const { navigation } = this.props;
    return (
      <Container
        toolbar={(
          <ToolBar
            iconLeft="arrowleft"
            onPressIconLeft={() => navigation.goBack()}
            parentTitle={Strings._reward}
          />
        )}
        bottomComponent={(
          <Button
            titleBtn={'Order'}
            onPress={() => this.onPressOrder(navigation)}
            styleBtn={{ marginHorizontal: Screen.width(4), marginVertical: Screen.width(4) }}
          />
        )}
        scrollable
        ref={container => this._ = container}
      >
      <Text>
        {'Sub-Cart'}
      </Text>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
export default SubCartScreen;