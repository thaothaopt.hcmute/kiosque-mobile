import React, { Component } from "react";
import { StyleSheet, Text, View, FlatList } from "react-native";
import { connect } from "react-redux";
import { requestGetTypesBrand } from "../../../redux/actions/getTypesBrandActions";
import Strings from "../../../contants/strings";
import ItemOptionH from "../../../components/ItemOptionH";
import Container from "../../../components/Container";
import BottomCart from '../../../components/BottomCart';
import ToolBar from '../../../components/ToolBar';
import Card from "../../../components/Card";
import Colors from '../../../contants/colors';
import Screen from '../../../utils/screen';
import Storage from '../../../utils/storage';
import { requestInitCart } from '../../../redux/actions/requestCartActions';

class MenuMainScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    }
  // fetch du lieu tu server
  componentDidMount() {
    const {
      navigation,
      dispatchRequestGetTypesBrand,
      dispatchInitCart
    } = this.props;

    if (navigation.state.params && navigation.state.params.id) {
      dispatchRequestGetTypesBrand(navigation.state.params.id);
    } 
    Storage.getData("cart", data => {
      if (data) {
        dispatchInitCart(data);
      } else {
        return [];
      }
    });
  }
  // onPressItemType = (item) => {
  // const { navigation } = this.props;
  //  navigation.navigate('DrinkListScreen', {id: item._id, typename: item.typename });
  //   // truyen contribute cua item
  // }

  _renderItem = ({item}) => {
    return(
    <Card
    typename = {item.typename}
    typeImage = {item.typeImage}
    onPressItemType={() => this.onPressItemType(item)}
    />
  );
  };

  render() {
    const { getTypesBrand, getUserInfo, navigation } = this.props;
    console.warn('type:',getTypesBrand.data)
    return (
      <Container
        style={styles.container}
        loading={getTypesBrand.isFetching}
        bottomComponent={( <BottomCart navigation={navigation} /> )}
      > 
        {getTypesBrand.isSuccess && getTypesBrand.data ? (
          <FlatList
            style={{ paddingTop: "3%",paddingLeft: "3%", paddingRight: "3%" }}
            data={getTypesBrand.data.typesBrand}
            keyExtractor={(item, index) => index.toString()}
            renderItem={item => this._renderItem(item)}
            numColumns={2}
          />
          
        ) : (
          <View /> 
        )}
      </Container>
     
    );
  }
} 

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    paddingLeft: "3%",
    paddingRight: "3%"
  },
  welcome: {
    fontSize: 20,
    margin: 10
  },
  instructions: {
    color: "#000000",
    marginBottom: 5
  },
  list: {
    justifyContent: "center",
    flexDirection: "row",
    flexWrap: "wrap",
    width: "100%",
    height: "100%",
    paddingTop: "10%",
    paddingBottom: "10%"
  },
  item: {
    backgroundColor: "#CCC",
    margin: 10,
    width: "90%",
    //  height: "30%",
    textAlign: "center",
    padding: "5%",
    //paddingLeft: "5%",
    //paddingRight: "5%",
    fontSize: 15,
    borderWidth: 1
  },
  sectionTitle: {
    color: Colors.TEXT_COLOR,
    fontSize: Screen.width(4),
  }
});

const mapStateToProps = state => ({
  getTypesBrand: state.getTypesBrand,
 // login: state.login,
  //getUserInfo: state.getUserInfo,
  //getProducts: state.getProducts,
});

const mapDispatchToProps = dispatch => ({
  dispatchRequestGetTypesBrand: data =>
  dispatch(requestGetTypesBrand(data)),
  //dispatchRequestGetTypes: () => dispatch(requestGetTypes()),
  dispatchInitCart: data => dispatch(requestInitCart(data))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MenuMainScreen);
