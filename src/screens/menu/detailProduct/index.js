import React, { Component } from 'react';
import { StyleSheet, Text, View, FlatList,Image } from "react-native";
import { connect } from "react-redux";
import { requestGetProducts } from "../../../redux/actions/getProductsActions";
import Strings from "../../../contants/strings";
import Header from "../../../components/Header";
import Container from "../../../components/Container";
import ListCard from "../../../components/ListCard";
import { Item } from 'native-base';

class DetailProductScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      //dataSource:[]
    };
  }
  componentDidMount() {
    const { navigation,dispatchRequestGetProducts } = this.props;
  
    dispatchRequestGetProducts(navigation.state.params.id);
  }

componentWillReceiveProps(nextProps) {
    const { getProducts } = nextProps;
    if (getProducts.isSuccess && getProducts.data) {
      this.setState({ dataSource: getProducts.data });
    }
  }
  _renderItem = ({ item }) => {
  
    return(
    // <ListCard style={{color:'white'}}
    //     // productImage ={item.productImage}
    //     // productName = {item.name}
    //     // productPrice = {item.price}
    //     //onPressItemProduct={() => this.onPressItemProduct(item)}
    //  />

     <View/>
  );
  
  };
  render() {
       //const { navigation, getProducts } = this.props;
    const { getProducts} = this.props;
 //   console.warn('data: ', getProducts.data)
    return (
    //   <Container
    //   style={styles.container}
    //   loading={getProducts.isFetching}
    
    // >
    //  {getProducts.isSuccess && getProducts.data ? (
    //     <FlatList
    //       style={{ paddingTop: "3%",paddingLeft: "3%", paddingRight: "3%" }}
    //       data={getProducts.data.products}
    //       keyExtractor={(item, index) => index.toString()}
    //        renderItem={item => this._renderItem(item)}
    //               numColumns={1}
    //     extraData={this.state}
    //     />
    //         ) : (
    //     <View />
    // )}
    // </Container>
<View style={styles.container}>
         <Text style={styles.welcome}>
          {'DetailProductScreen'}
        </Text>
</View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
const mapStateToProps = state => ({
  GetProducts: state.GetProducts,
  login: state.login,
  getUserInfo: state.getUserInfo
});

const mapDispatchToProps = dispatch => ({
  dispatchRequestGetProducts: (data) => dispatch(requestGetProducts(data))
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DetailProductScreen);
