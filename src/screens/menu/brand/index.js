import React, { Component } from "react";
import { StyleSheet, Text, View, FlatList } from "react-native";
import { connect } from "react-redux";
import { requestGetBrands } from "../../../redux/actions/getBrandsActions";
import Strings from "../../../contants/strings";
import ItemOptionH from "../../../components/ItemOptionH";
import Container from "../../../components/Container";
import BottomCart from '../../../components/BottomCart';
import Card from "../../../components/Card";
import Colors from '../../../contants/colors';
import Screen from '../../../utils/screen';
import Storage from '../../../utils/storage';
import { requestInitCart } from '../../../redux/actions/requestCartActions';

class MainBrand extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  
  }
  // fetch du lieu tu server
  componentDidMount() {
    const { dispatchRequestGetBrands, dispatchInitCart } = this.props;
    dispatchRequestGetBrands();
    Storage.getData('cart', (data) => {
      if (data) {
        dispatchInitCart(data);
      } else {
        return [];
      }
    });
  }

  onPressItemType = (item) => {
  const { navigation } = this.props;
   navigation.navigate('MenuMainScreen',{id: item._id});
    // truyen contribute cua item
  }

  _renderItem = ({item}) => {
    return(
    <Card
    typename = {item.brandname}
    typeImage = {item.brandImage}
    onPressItemType={() => this.onPressItemType(item)}
    />
  );
  };

  render() {
    const { getBrands,navigation } = this.props;
    return (
      <Container
        style={styles.container}
        loading={getBrands.isFetching}
        bottomComponent={(
          <BottomCart
            navigation={navigation}
          />
        )}
      >
        {getBrands.isSuccess && getBrands.data ? (
          <FlatList
          ListHeaderComponent={(
            <View style={{ marginBottom: Screen.width(4)}}>
            <ItemOptionH
              title={Strings.menu_brand_title}
              style={styles.sectionTitle}
              disabled
            />
            </View>
          )}
            style={{ paddingTop: "3%",paddingLeft: "3%", paddingRight: "3%" }}
            data={getBrands.data}
            keyExtractor={(item, index) => index.toString()}
            renderItem={item => this._renderItem(item)}
            numColumns={2}
          />
          
        ) : (
          <View />
        )}
      </Container>
     
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    paddingLeft: "3%",
    paddingRight: "3%"
  },
  welcome: {
    fontSize: 20,
    margin: 10
  },
  instructions: {
    color: "#000000",
    marginBottom: 5
  },
  list: {
    justifyContent: "center",
    flexDirection: "row",
    flexWrap: "wrap",
    width: "100%",
    height: "100%",
    paddingTop: "10%",
    paddingBottom: "10%"
  },
  item: {
    backgroundColor: "#CCC",
    margin: 10,
    width: "90%",
    //  height: "30%",
    textAlign: "center",
    padding: "5%",
    //paddingLeft: "5%",
    //paddingRight: "5%",
    fontSize: 15,
    borderWidth: 1
  },
  sectionTitle: {
    color: Colors.TEXT_COLOR,
    fontSize: Screen.width(4),
  }
});

const mapStateToProps = state => ({
  getBrands: state.getBrands,
//   login: state.login,
//   getUserInfo: state.getUserInfo
});

const mapDispatchToProps = dispatch => ({
  dispatchRequestGetBrands: () => dispatch(requestGetBrands()),
  dispatchInitCart: data => dispatch(requestInitCart(data))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MainBrand);
