import React, { Component } from "react";
import { StyleSheet, View, FlatList, Image, Text } from "react-native";
import { connect } from "react-redux";
import { requestGetProductsBrand } from "../../../redux/actions/getProductsBrandActions";
import Strings from "../../../contants/strings";
import Container from "../../../components/Container";
import BottomCart from "../../../components/BottomCart";
import ToolBar from "../../../components/ToolBar";
import ListCard from "../../../components/ListCard";
import colors from "../../../contants/colors";
import Screen from "../../../utils/screen";
import ModalDetailProduct from "../../../components/modalDetailProduct";
import { requestGetProducts } from "../../../redux/actions/getProductsActions";
import Storage from "../../../utils/storage";
import { requestInitCart } from "../../../redux/actions/requestCartActions";

class DrinkBrand extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalDetailProduct: false,
      item: {},
      query: "",
      
    };
  }

  componentDidMount() {
    const {
      navigation,
      dispatchRequestGetProductsBrand,
      dispatchRequestGetProducts,
      dispatchInitCart
    } = this.props;

    if (navigation.state.params && navigation.state.params.id) {
      dispatchRequestGetProductsBrand(navigation.state.params.id);
    } else {
      dispatchRequestGetProducts();
    }
    Storage.getData("cart", data => {
      if (data) {
        dispatchInitCart(data);
      } else {
        return [];
      }
    });
  }

  setModalDetailProduct = (e, item) => {
    this.setState({
      modalDetailProduct: e,
      item
    });
  };
  // _renderItem = ({ item }) => {
  //   return (
  //     <ListCard
  //       style={{ color: "white" }}
  //       productImage={item.productImage}
  //       productName={item.name}
  //       productPrice={item.price}
  //       onPressItemProduct={() => this.setModalDetailProduct(true, item)}
  //     />
  //   );
  // };

  render() {
    const { getProductsBrand, navigation, getProducts } = this.props;
    const { modalDetailProduct, item } = this.state;
    let data = [];
    //console.warn('>> ', getProductsType.data);
    return (
      <Container
        style={styles.container}
        toolbar={
          <ToolBar
            iconLeft="arrowleft"
            onPressIconLeft={() => navigation.goBack()}
            title
           // parentTitle={navigation.state.params.typename}
          />
        }
        loading={getProductsBrand.isFetching}
        bottomComponent={<BottomCart navigation={navigation} />}
      >
        {getProductsBrand.isSuccess && getProductsBrand.data ? (
          <FlatList
            data={
              navigation.state.params.id
                ? getProductsBrand.data.productsBrand
                : getProducts.data
            }
            keyExtractor={(item, index) => index.toString()}
            renderItem={item => this._renderItem(item)}
            numColumns={1}
          />
        ) : (
          <View />
        )}
        {/* <ModalDetailProduct
          product={item}
          closeModal={() => this.setModalDetailProduct(false, {})}
          modalVisible={modalDetailProduct}
        /> */}
        

      </Container>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    backgroundColor: colors.SECOND_BACKGROUND_COLOR
  },
  welcome: {
    fontSize: 20,
    margin: 10
  },
  instructions: {
    color: "#000000",
    marginBottom: 5
  },
  list: {
    justifyContent: "center",
    flexDirection: "row",
    flexWrap: "wrap",
    width: "100%",
    height: "100%",
    paddingTop: "10%",
    paddingBottom: "10%"
  },
  item: {
    backgroundColor: "white",
    margin: 10,
    width: "90%",
    //  height: "30%",
    textAlign: "center",
    padding: "5%",
    fontSize: 15,
    borderWidth: 1
  }
});
const mapStateToProps = state => ({
  getProductsBrand: state.getProductsBrand,
  login: state.login,
  getUserInfo: state.getUserInfo,
  getProducts: state.getProducts,
 
});

const mapDispatchToProps = dispatch => ({
  dispatchRequestGetProductsBrand: data =>
  dispatch(requestGetProductsBrand(data)),
  dispatchRequestGetProducts: () => dispatch(requestGetProducts()),
  dispatchInitCart: data => dispatch(requestInitCart(data))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DrinkBrand);
