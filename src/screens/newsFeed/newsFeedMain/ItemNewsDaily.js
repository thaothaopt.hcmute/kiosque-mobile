import React, { Component } from 'react';
import {
  StyleSheet, TouchableOpacity,
  Text,
  View, Image, Dimensions
} from 'react-native';
import * as Configs from '../../../redux/service/config';
import Colors from '../../../contants/colors';
import Screen from '../../../utils/screen';

const { width: screenWidth, height: screenHeight } = Dimensions.get('window');
class ItemNewsDaily extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }
  render() {
      const {promotionImage, title , src, description, onPressItem, styleImg, styleContain } = this.props;
      const img = `${Configs.PICTURE}${src}`;

    return (
      <TouchableOpacity style={styleContain || styles.container}
        onPress={onPressItem}
      >
      {/* <Image
      style={ styleImg || {
        height: screenWidth - 200,
        marginTop: -20,
        
      }}
      resizeMode='cover'
        source={{uri: img }}
      /> */}
         <Image  style={ styleImg || {
        height: screenWidth - 150,
        marginTop: -10,
       
      }}
      resizeMode='cover' source={{uri: promotionImage}} />

        <Text style={styles.title}>
          {title}
        </Text> 
        <Text
        style={styles.content}
          numberOfLines={2}
        >
          {description}
        </Text>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: screenWidth - 155,
    justifyContent: 'flex-start',
    flexDirection: 'column',
    marginHorizontal: 5,
    marginTop: Screen.width(4),
    backgroundColor: Colors.PRIMARY_COLOR,
    borderRadius: 5
  },
  title: {
    fontSize: Screen.width(4),
    fontWeight: '500',
    color: Colors.DISABLE_COLOR,
    paddingHorizontal: Screen.width(3),
    paddingTop: 5
    },
    content: {
      fontSize: Screen.width(3.5),
      color: Colors.DISABLE_COLOR,
      paddingHorizontal: 10,
      paddingBottom: 5
    }
});
export default ItemNewsDaily;