import React from 'react';
import ToolBar from '../../../components/ToolBar';

export const renderNewsFeedToolbar = (onPressIconLeft, onPressIconRight) => (
  <ToolBar
  iconLeft="close"
  onPressIconLeft={onPressIconLeft}
  iconRight="send-o"
  onPressIconRight={onPressIconRight}
  />
);

export const listSales = [
  {
    id: 1,
    title: 'Sale 001',
    src: require('../../../assets/images/sale001.png')
  },
  {
    id: 2,
    title: 'Sale 002',
    src: require('../../../assets/images/sale002.jpg')
  },
  {
    id: 3,
    title: 'Sale 003',
    src: require('../../../assets/images/sale003.jpg')
  },
  {
    id: 4,
    title: 'Sale 004',
    src: require('../../../assets/images/sale004.jpg')
  },
  {
    id: 5,
    title: 'Sale 005',
    src: require('../../../assets/images/sale005.jpg')
  }
]
