import React, { Component } from 'react';
import {
  StyleSheet, TouchableOpacity, Text, FlatList, View, Dimensions
} from 'react-native';
import { connect } from 'react-redux';
import Carousel from 'react-native-snap-carousel';
import Empty from '../../../components/Empty';
import Strings from '../../../contants/strings';
import Colors from '../../../contants/colors';
import Header from '../../../components/Header';
import Dash from '../../../components/Dash';
import Container from '../../../components/Container';
import ItemOptionH from '../../../components/ItemOptionH';
import { renderNewsFeedToolbar, listSales } from './helper';
import { requestGetUserInfo } from '../../../redux/actions/getUserInfoActions';
import { requestGetNewsDaily } from '../../../redux/actions/getNewsDailyActions';
import { requestGetNewsEvent } from '../../../redux/actions/getNewsEventActions';
import { requestGetProducts } from '../../../redux/actions/getProductsActions';
import ItemNewsDaily from './ItemNewsDaily';
import Screen from '../../../utils/screen';

const { width: screenWidth, height: screenHeight } = Dimensions.get('window');
const styles = StyleSheet.create({
  sectionTitle: {
    color: 'black',
    fontSize: Screen.width(4),
    marginLeft: Screen.width(3),
  }
});
class NewsFeedMainScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeSlide: 1,
      dataSource: {},
//title:''
    }
  };

  componentDidMount() {
    const { login, dispatchGetUserInfo, getNewsDaily, getNewsEvent, dispatchRequestGetProducts } = this.props;
    if (login.logged) {
      dispatchGetUserInfo(login.auth._id);
    }
    if (!getNewsDaily.isSuccess || !getNewsEvent.isSuccess) {
      this.dispatchGetNews('daily');
      this.dispatchGetNews('event');
    }
    dispatchRequestGetProducts();
  }

  dispatchGetNews = (e) => {
    const {
      dispatchRequestGetNewsDaily,
      dispatchRequestGetNewsEvent
    } = this.props;
    if (e === 'daily') {
      dispatchRequestGetNewsDaily(e);
    } else {
      dispatchRequestGetNewsEvent(e);
    }
  }

  onPressUserPicture = () => {
    const { navigation, login } = this.props;
    if (login.logged) {
      navigation.navigate('EditProfileScreen');
    } else {
      navigation.navigate('LogInScreen');
    }
  }

  onPressToOrder = () => {

  }

  onPressItem = (item) => {
    //console.warn('info: ', item.title);
    const { navigation } = this.props;
    navigation.navigate('DetailNewsScreen',{item});

  }

  onPressIconRight = () => {
    const { navigation } = this.props;
    navigation.navigate('NotificationScreen');
  }

  renderNewsDaily = ({ item }) => {
    return (
      <ItemNewsDaily
        title={item.title}
        promotionImage={item.promotionImage}
         
        description={item.description}
        onPressItem={() => this.onPressItem(item)}
      />
    );
  }

  renderNewsEvent = ({ item }) => {
    return (
    <ItemNewsDaily
    styleContain={{ width: '100%', marginTop: Screen.width(4), backgroundColor: Colors.PRIMARY_COLOR, borderRadius: 5}}
        title={item.title}
        promotionImage={item.promotionImage}
        description={item.description}
        onPressItemDaily={() => this.onPressItem(item)}
      />
      );
  }

  renderToolbar = () => renderNewsFeedToolbar(() => {
    this.onPressIconLeft
  },
    this.onPressIconRight
  );

  render() {
    const { getNewsDaily, getUserInfo, getNewsEvent } = this.props;
    return (
      <Container
        loading={getNewsDaily.isFetching || getNewsEvent.isFetching}
        toolbar={(
          <Header
            count={10}
            notify
            iconRight="bells"
            onPressIconRight={() => this.onPressIconRight()}
            onPressUserPicture={() => this.onPressUserPicture()}
            username={getUserInfo.userinfo.username || Strings.LOGIN}
            money={getUserInfo.userinfo.rewards}
            image={{ height: Screen.width(14), width: Screen.width(14), borderRadius: Screen.width(7) }}
            userPicture={getUserInfo.userinfo.userImage || ''}
          />
        )}
        scrollable
        ref={component => this._ = component}
      >
        {(getNewsDaily.data && getNewsEvent.data)
          ? (
            <View style={[{ margin: Screen.width(3) }]}>
              <Dash />
              <ItemOptionH
                title={Strings.SALES}
                style={styles.sectionTitle}
                disabled={true}
              />
               <Dash />
              <Carousel
                loop={true}
                autoplay={true}
                autoplayDelay={500}
                ref={ref => this.carouselRef = ref}
                data={getNewsDaily.data}
                onSnapToItem={i => this.setState({ activeSlide: i })}
                inactiveSlideScale={1}
                inactiveSlideOpacity={1}
                renderItem={this.renderNewsDaily}
                sliderWidth={screenWidth - 20}
                itemWidth={250}
              />
              <Dash style={{ marginTop: Screen.width(6) }} />
              <ItemOptionH
                title={Strings.FRESH}
                style={styles.sectionTitle}
                disabled={true}
              />
               <Dash />
              <FlatList
                data={getNewsEvent.data}
                renderItem={this.renderNewsEvent}
                keyExtractor={(item, key) => key.toString()}
                extraData={this.state}
              />
            </View>
          )
          : (<Empty warn={Strings.RECONNECT}/>)
        }
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  login: state.login,
  getNewsDaily: state.getNewsDaily,
  getNewsEvent: state.getNewsEvent,
  getUserInfo: state.getUserInfo
});

const mapDispatchToProps = dispatch => ({
  dispatchGetUserInfo: (data) => dispatch(requestGetUserInfo(data)),
  dispatchRequestGetNewsDaily: (data) => dispatch(requestGetNewsDaily(data)),
  dispatchRequestGetNewsEvent: (data) => dispatch(requestGetNewsEvent(data)),
  dispatchRequestGetProducts: () => dispatch(requestGetProducts())
});

export default connect(mapStateToProps, mapDispatchToProps)(NewsFeedMainScreen);