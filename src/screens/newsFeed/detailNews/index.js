import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View, FlatList, Image,ImageBackground
} from 'react-native';
import { connect } from "react-redux";
import { requestGetNewsDaily } from "../../../redux/actions/getNewsDailyActions";
import ListCard from "../../../components/ListCard";
import Container from "../../../components/Container";
import { Item } from 'native-base';
import Screen from '../../../utils/screen';
import Strings from "../../../contants/strings";
import Card from "../../../components/Card"
import colors from '../../../contants/colors';
class DetailNewsScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }
 
  componentDidMount() {
    const { navigation, dispatchRequestGetNewsDaily } = this.props;
    dispatchRequestGetNewsDaily(navigation.state.params.item);
    
  }

  render() {
    const { getNewsDaily } = this.props;
    return (
      <View style={styles.container}>
       <ImageBackground style={styles.img}  resizeMode= 'stretch' source={{uri: this.props.navigation.state.params.item.promotionImage}} >
<View style={{paddingBottom:'80%'}}/>
       <Text style={styles.titleText}>{this.props.navigation.state.params.item.title}</Text>
      </ImageBackground>
      <Text style={styles.des}>{this.props.navigation.state.params.item.description}</Text>


      </View>
      
    );
  }
 
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //justifyContent: 'center',
    //alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  titleText:{
    fontSize:24,
    color: '#ffffff',
    backgroundColor: 'rgba(10,10,10, 0.8)',
    //borderTopColor:'#000000',
  //  borderTopWidth: 2,
   // letterSpacing:4,
    paddingTop:'3%',
    paddingBottom:'3%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  img:{
    width: "100%",
    height:"70%",
    // borderTopLeftRadius: 5,
    // borderTopRightRadius: 5,
  },
  des:{
    color:'black',
    padding:'3%',
    alignItems: 'center',
    justifyContent: 'center',
    fontSize:15,
  }
});
const mapStateToProps = state => ({
  getNewsDaily: state.getNewsDaily,
  login: state.login,
  getUserInfo: state.getUserInfo,
});

const mapDispatchToProps = dispatch => ({
  dispatchRequestGetNewsDaily: (data) => dispatch(requestGetNewsDaily(data))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DetailNewsScreen);

