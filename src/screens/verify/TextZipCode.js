import React, { Component } from 'react';
import {
  StyleSheet, TouchableOpacity,
  Text, TextInput,
  View
} from 'react-native';
import Colors from '../../contants/colors';


class TextZipCode extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }


  render() {
    const { timeLeft, onPressResend, onChangeText } = this.props;
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          {`Verify code will be sent in ${timeLeft}s.\n Zip code has 4 letter.`}
        </Text>

        <View style={{ flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
          <TextInput
            onChangeText={onChangeText}
            underlineColorAndroid={Colors.TRANSPARENT}
            style={{ fontSize: 35, letterSpacing: 18, color: Colors.PRIMARY_COLOR }}
            maxLength={4}
            autoFocus={true}
            selectionColor={Colors.PRIMARY_COLOR}
          />
          <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 10 }}>
            <View style={styles.dash} />
            <View style={styles.dash} />
            <View style={styles.dash} />
            <View style={styles.dash} />
          </View>
        </View>
        <TouchableOpacity
          onPress={onPressResend}
          style={{ marginVertical: 15}}
        >
          <Text style={{ color: Colors.PRIMARY_COLOR }}>
            {'Resend verify code.'}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  welcome: {
    textAlign: 'center',
    margin: 10,
  },
  dash: {
    height: 3,
    backgroundColor: Colors.PRIMARY_COLOR,
    width: 30,
    marginRight: 7
  }
});
export default TextZipCode;