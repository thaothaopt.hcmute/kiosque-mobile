import React, { Component } from 'react';
import {
  StyleSheet, View
} from 'react-native';
import RNAccountKit from 'react-native-facebook-account-kit'
import TextZipCode from './TextZipCode';
import Container from '../../components/Container';
import ToolBar from '../../components/ToolBar';
import Strings from '../../contants/strings';

class VerifyScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      zipcode: '',
      timeLeft: 56
    };
  }

  onPressResend = (zipcode) => {
    console.warn('>>', zipcode);
  }

  render() {
    RNAccountKit.loginWithPhone()
  .then((token) => {
    if (!token) {
      console.log('Login cancelled')
    } else {
      console.log(`Logged with phone. Token: ${token}`)
    }
  })
    RNAccountKit.configure({
      responseType: 'code', // 'token' by default,
      receiveSMS: true,
      // titleType: 'login',
      // initialAuthState: '',
      // initialEmail: 'some.initial@email.com',
      initialPhoneCountryPrefix: '+1', // autodetected if none is provided
      // initialPhoneNumber: '123-456-7890',
      // countryWhitelist: ['AR'], // [] by default
      // countryBlacklist: ['US'], // [] by default
      defaultCountry: 'AR',
    })
    
    const { navigation } = this.props;
    const { timeLeft } = this.state;
    return (
      <Container
      toolbar={(
        <ToolBar
        iconLeft="arrowleft"
        onPressIconLeft={() => navigation.goBack()}
        parentTitle={Strings.VERIFYPHONE}
        />
      )}
        scrollable
        ref={component => this._ = component}
      >
        {/* <TextZipCode
          timeLeft={timeLeft}
          onPressResend={() => this.onPressResend()}
          onChangeText={(text) => this.setState({ zipcode: text})}
        /> */}
        <View />
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  }
});
export default VerifyScreen;