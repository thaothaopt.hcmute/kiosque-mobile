import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image
} from 'react-native';
import { connect } from 'react-redux';
import AntDesign from 'react-native-vector-icons/AntDesign';
import AddToCart from '../../components/modalDetailProduct/AddToCart';
import Colors from '../../contants/colors';
import Screen from '../../utils/screen';
import Storage from '../../utils/storage';
import { requestUpdateCart } from '../../redux/actions/requestCartActions';

class Item extends Component {
  constructor(props) {
    super(props);
    this.state = {
      total: 0,
      quantity: 0,
      datacarts: []
    };
  }

  componentWillMount() {
    const { total, quantity } = this.props;
    this.setState({ total: total, quantity: quantity });
    Storage.getData('cart', (data) => {
      if (data) {
        this.setState({ datacarts: data });
      } else {
      }
    });
  }

  componentWillReceiveProps(nextProps) {
    const { requestCart } = nextProps;
    if(requestCart.selected) {
      this.setState({ datacarts: requestCart.cart });
    }
  }

  onPressMinus = (e, productid, price, size, useReward, reward) => {
    const { dispatchRequestCart, requestCart } = this.props;
    const { datacarts } = this.state;
      this.setState({ quantity: e - 1});
      if (requestCart.cart.length > 0) {
        datacarts.forEach((element, index) => {
          if(element.product.productid === productid) {
            element.quantity = e - 1;
            element.total = useReward ? (((price * (e - 1)) + ((size === 'L')
            ? (10000 * quantity) : 0)) - reward) : ((price * (e - 1)) + ((size === 'L')
            ? (10000 * quantity) : 0))
          }
        });
      dispatchRequestCart(datacarts);
      }
  }

  onPressPlus = (e, productid, price, size, useReward, reward) => {
    const { dispatchRequestCart, requestCart } = this.props;
    const { datacarts } = this.state;
    this.setState({ quantity: e + 1});
    if (requestCart.cart.length > 0) {
      datacarts.forEach((element, index) => {
        if(element.product.productid === productid) {
          element.quantity = e + 1;
          element.total = useReward ? (((price * (e + 1)) + ((size === 'L')
          ? (10000 * quantity) : 0)) - reward) : ((price * (e + 1)) + ((size === 'L')
          ? (10000 * quantity) : 0))
        }
      });
      dispatchRequestCart(datacarts);
    }
  }

  

  render() {
    const { quantity } = this.state;
    const {
      name, productImage, size, price, productid, onPressDeleteItem,useReward, reward
    } = this.props;
    return (
      <View style={{
        margin: 15, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
      <Image
        source={{uri: productImage}}
        style={{
          width: Screen.width(23),
          height: Screen.width(23),
          borderColor: Colors.PRIMARY_COLOR,
          borderWidth: 0.5,
          borderRadius: 3
        }}
      />
      <View style={{ flexDirection: 'column' }}>
        <Text style={{ color: Colors.TEXT_COLOR }}>
          {name}
        </Text>
        <Text style={{ color: Colors.TEXT_COLOR_2, paddingVertical: 3 }}>
          {`Size: ${size}`}
        </Text>
        <AddToCart
        onPressMinus={() => this.onPressMinus(quantity, productid, price, size, useReward, reward)}
        quantity={quantity}
        onPressPlus={() => this.onPressPlus(quantity, productid, price, size,useReward, reward)}
        />
        </View>
        <View style={{ flexDirection: 'column' }}>
          <View style={{ position: 'absolute', top: 0, right: 0}}>
            <TouchableOpacity
              onPress={onPressDeleteItem}
            >
              <AntDesign
                name="close"
                size={15}
                color={Colors.TEXT_COLOR_2}
              />
          </TouchableOpacity>
          </View>
        <View style={{ height: 65 }}/>
        <Text style={{ color: Colors.ERROR_COLOR }}>
          {` ${(price * quantity) + ((size === 'L')
                  ? (10000 * quantity) : 0)} vnd`}
        </Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

const mapStateToProps = state => ({
  getProducts: state.getProducts,
  requestCart: state.requestCart
});

const mapDispatchToProps = dispatch => ({
  dispatchRequestCart: (data) => dispatch(requestUpdateCart(data))

});
export default connect(mapStateToProps, mapDispatchToProps) (Item);