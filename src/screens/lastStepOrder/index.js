import React, { Component } from 'react';
import {
  StyleSheet, TouchableOpacity,
  Text,
  View,
  FlatList
} from 'react-native';
import { connect } from 'react-redux';
import Location from './Location';
import Container from "../../components/Container";
import Strings from '../../contants/strings';
import ToolBar from '../../components/ToolBar';
import Dash from '../../components/Dash';
import ItemOptionH from '../../components/ItemOptionH';
import BottomSubTotal from '../../components/BottomSubTotal';
import BlockOrderInfo from '../../components/BlockOrderInfo';
import EmptyCart from '../../components/EmptyCart';
import BottomInputDiscount from '../../components/BottomInputDiscount';
import TextBox from '../../components/TextBox';
import Item from './Item';
import Reward from './Reward';
import Colors from '../../contants/colors';
import Screen from '../../utils/screen';
import Storage from '../../utils/storage';
import { requestUpdateCart } from '../../redux/actions/requestCartActions';
import { requestGetAddress } from '../../redux/actions/getAddressActions';
import {
  requestOrder,
  requestCreateReward,
  requestUpdateReward
} from '../../redux/service/api';
import { requestGetReward } from '../../redux/actions/getRewardActions';

class LastStepOrderScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      phone: '',
      dicountcode: '',
      total: 0,
      reward: 0,
      useReward: false,
      datacarts: [],
      emptycart: false,
      isEdit: false,
      address: 'No. 5/6/7, Linh Trung, Thu Duc'
    };
  }

  componentDidMount() {
    const { dispatchRequestCart,
      login, dispatchGetAddresses,
      getUserInfo, dispatchRequestGetReward
    } = this.props;
    Storage.getData('cart', (data) => {
      if (data) {
        dispatchRequestCart(data)
      } else {
      }
    });
    if (login.logged) {
      dispatchGetAddresses(login.auth._id);
      this.setState({
        username: getUserInfo.userinfo.username,
        phone: login.auth.phone
      });
    dispatchRequestGetReward(login.auth._id);
    }
  }

  componentWillReceiveProps(nextProps) {
    const { requestCart, getReward } = nextProps;
    if (requestCart.selected && requestCart.cart.length > 0) {
      this.setState({
        datacarts: requestCart.cart,
        total: this.sumTotal(requestCart.cart, 0)
      });
    }
    if (getReward.isSuccess) {
      this.setState({
        reward: getReward.reward
      });
    }
  }

  onPressOrder = (navigation, datacarts) => {
    const { address, phone, username, total, discountcode, useReward } = this.state;
    const { login, getReward } = this.props;
    const rewardDataUpdate = {
      authid: login.logged ? login.auth._id : '',
      params: [
        {
          key: 'reward',
          value: useReward ? this.countReward(total, 0)
            : this.countReward(total, 0) + getReward.reward,
        }
      ]
    };

    let orderData = {
      recipiant: username,
      phone: phone,
      total: total,
      address: address,
      orderlist: datacarts,
      discountcode: discountcode,
      discountvalue: this.discountvalue(discountcode, 0),
      status: -2,
      dateoforder: new Date(),
      useReward: useReward,
      reward: useReward ? getReward.reward : 0
    };
    if (login.logged) {
      orderData = { ...orderData, authid: login.auth._id };
      if (getReward.isSuccess && getReward.data.length === 0) {
        requestCreateReward({
          authid: login.auth._id,
          reward: 0
        }).then((res) => {
        }).catch((error) => {
          console.warn('cre==error== ', error);
        });
      } else {
        requestUpdateReward(rewardDataUpdate).then((res) => {
        }).catch((error) => {
          console.warn('update:error== ', error);
        });
      }
    }
    this._.showLoading();
    requestOrder(orderData).then((res) => {
      console.warn('res== ', res);
      if (res.data && res.data.status) {
        navigation.navigate('OrderCompleteScreen');
      } else {
        // this._.showMessage()
      }
      this._.hideLoading();
    }).catch((error) => {
      console.warn('res== ', error);
    });
  }

  onPressOrderNow = (navigation) => {
    navigation.navigate('DrinkListScreen', { typename: Strings.menu_all_drinks_title });
  }

  onPressDeleteItem = (productid) => {
    this._.showLoading();
    const { dispatchRequestCart } = this.props;
    const { datacarts } = this.state;
    datacarts.forEach((element, index) => {
      if (element.product.productid === productid) {
        datacarts.splice(index, 1);
      }
    });
    dispatchRequestCart(datacarts);
    this._.hideLoading();
    Storage.setData('cart', datacarts);

  }

  onPressBox = (e) => {
    const { total, useReward } = this.state;
    const { getReward } = this.props;
    this.setState({
      useReward: !e,
      total: !e ? total - getReward.reward : total + getReward.reward,
      dicountcode: ''
    });
  }

  /** functions */

  sumTotal = (carts, total) => {
    carts.forEach((element, index) => {
      total += element.total;
    })
    return total;
  }

  countReward = (total, value) => {
    const tempt = Math.floor(total / 50000);
    if (tempt >= 1) {
      value = tempt * 1000;
    } else {
      value = 0;
    }
    return value;
  }

  discountvalue = (dicountcode, value) => {
    return value;
  }

  renderOrderItem = ({ item }, useReward, reward) => {
    return (
      <Item
        useReward={useReward}
        reward={reward}
        name={item.product.name}
        productImage={item.product.productImage}
        total={item.total}
        quantity={item.quantity}
        size={item.size}
        price={item.product.price}
        productid={item.product.productid}
        onPressDeleteItem={() => this.onPressDeleteItem(item.product.productid)}
      />
    );
  }

  render() {
    const {
      navigation, getReward, getAddresses, login, getUserInfo
    } = this.props;
    const {
      total, datacarts, username, dicountcode, phone, isEdit, address, useReward, reward
    } = this.state;

    return (
      <Container
        toolbar={(
          <ToolBar
            iconLeft="arrowleft"
            onPressIconLeft={() => navigation.goBack()}
            parentTitle={Strings.YOUR_CART}
          />
        )}
        bottomComponent={(datacarts.length > 0) ? (
          <BottomSubTotal
            onPress={() => this.onPressOrder(navigation, datacarts)}
            total={total}
          />
        ) : (<View />)}
        scrollable
        ref={container => this._ = container}
      >
        {(datacarts.length > 0)
          ? (
            <View style={{ backgroundColor: Colors.SECOND_BACKGROUND_COLOR }}>
              <View style={styles.containerInfo}>
                <BlockOrderInfo
                  sectionTitle={'Information'}
                  extraComponent={(
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }} >
                      <View style={{ flex: 0.45 }}>
                        <TextBox
                          icon="user"
                          placeholder={(login.logged) ? getUserInfo.userinfo.username : 'Your name'}
                          onChangeText={(text) => this.setState({ username: text })}
                          selectionColor={Colors.TEXT_COLOR}
                          placeholderTextColor={Colors.PRIMARY_COLOR}
                          styleTextBox={{ fontSize: 13 }}
                        />
                      </View>
                      <View style={{ flex: 0.45 }}>
                        <TextBox
                          icon="phone"
                          placeholder={(login.logged) ? login.auth.phone : Strings.order_your_phone}
                          onChangeText={(text) => this.setState({ phone: text })}
                          selectionColor={Colors.TEXT_COLOR}
                          placeholderTextColor={Colors.PRIMARY_COLOR}
                          styleTextBox={{ fontSize: 13 }}
                        />
                      </View>
                    </View>
                  )}
                />
                <Location
                  onPressEditLocation={() => this.onPressEditLocation(isEdit)}
                  location={address}
                  onChangeText={text => this.setState({ address: text })}
                  isEdit={isEdit}
                />
              </View>
              {/* 
  Open when get data of Orders
  */}
              <View style={styles.containerInfo}>
                <FlatList
                  ListHeaderComponent={(
                    <ItemOptionH
                      title={Strings.INFO_DETAIL_ORDER}
                      style={styles.sectionTitle}
                      disabled
                    />
                  )}
                  data={datacarts || []}
                  renderItem={(item) => this.renderOrderItem(item, useReward, reward)}
                  ItemSeparatorComponent={() => <Dash style={{ marginVertical: 3 }} />}
                  keyExtractor={(item, key) => key.toString()}
                  ref={list => this.list = list}
                  ListFooterComponent={(
                    <View style={{ flexDirection: 'column' }}>
                    <Dash />
                      {!useReward
                        ? (
                          <BottomInputDiscount
                            onChangeText={text => this.setState({ dicountcode: text })}
                          />
                        )
                        : (<View />)}

                      {(login.logged && getReward.reward > 0 && dicountcode === '')
                        ? (
                          <Reward
                            reward={getReward.reward}
                            icon={useReward ? 'check-box' : 'check-box-outline-blank'}
                            checked={useReward}
                            onPressBox={() => this.onPressBox(useReward)}
                          />
                        )
                        : (<View />)}
                    </View>
                  )}
                />
              </View>
            </View>
          )
          : (
            <EmptyCart
              onPress={() => this.onPressOrderNow(navigation)}
            />)
        }
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  containerInfo: {
    backgroundColor: Colors.BACKGROUND_COLOR,
    marginVertical: 10,
    marginHorizontal: 10,
    padding: 10,
    borderColor: Colors.SECOND_COLOR,
    borderWidth: 0.7,
    borderRadius: 5
  },
  sectionTitle: {
    color: Colors.TEXT_COLOR,
    fontSize: Screen.width(4),
    marginLeft: 10,
  }
});

const mapStateToProps = state => ({
  login: state.login,
  getUserInfo: state.getUserInfo,
  requestCart: state.requestCart,
  getAddresses: state.getAddresses,
  getReward: state.getReward
});

const mapDispatchToProps = dispatch => ({
  dispatchRequestCart: (data) => dispatch(requestUpdateCart(data)),
  dispatchGetAddresses: (data) => dispatch(requestGetAddress(data)),
  dispatchRequestGetReward: (data) => dispatch(requestGetReward(data))
});
export default connect(
  mapStateToProps,
  mapDispatchToProps)(LastStepOrderScreen);