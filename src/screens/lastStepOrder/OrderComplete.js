import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View, ActivityIndicator
} from 'react-native';
import { connect } from 'react-redux';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Screen from '../../utils/screen';
import Colors from '../../contants/colors';
import Strings from '../../contants/strings';
import Storage from '../../utils/storage';
import { requestCleanCart, requestInitCart } from '../../redux/actions/requestCartActions';

class OrderCompleteScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  componentDidMount() {
    const { navigation } = this.props;
    this.cleanCartStorage();
    setTimeout(() => {
      navigation.navigate('NewsFeedStack');
    }, 2000);
  }

  cleanCartStorage = () => {
    Storage.removeData('cart');
    const { dispatchRequestCart, dispatchInitCart } = this.props;
    dispatchRequestCart();
    dispatchInitCart([]);
  }

  render() {
    return (
      <View style={[styles.container, { flexDirection: 'column' }]}>
        <FontAwesome5
          name="shipping-fast"
          color={Colors.PRIMARY_COLOR}
          size={Screen.width(10)}
        />
        <Text style={{ color: Colors.TEXT_COLOR, fontSize: Screen.width(5) }}>
          {Strings.ORDER_FINISHED}
        </Text>
        <Text style={{ color: Colors.TEXT_COLOR, fontSize: Screen.width(4) }}>
          {Strings.order_thank_for_order}
        </Text>
        <ActivityIndicator
          color={Colors.PRIMARY_COLOR}
          animating
          size="large"
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  }
});

const mapStateToProps = state => ({
  requestCart: state.requestCart
});

const mapDispatchToProps = dispatch => ({
  dispatchRequestCart: (data) => dispatch(requestCleanCart(data)),
  dispatchInitCart: data => dispatch(requestInitCart(data))
});
export default connect(
  mapStateToProps,
  mapDispatchToProps) (OrderCompleteScreen);