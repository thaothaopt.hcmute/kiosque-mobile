import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity
} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Colors from '../../contants/colors';
import Screen from '../../utils/screen';

class Reward extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }


  render() {
      const { reward, icon, checked, onPressBox } = this.props; 
    return (
        <View style={{ flexDirection: 'column', marginHorizontal: 15, marginTop: 15 }}>
      <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
        <Text style={{ fontSize: Screen.width(4), color: Colors.TEXT_COLOR }}>
          {'Use your reward?'}
        </Text>
        <TouchableOpacity
        onPress={onPressBox}
        >
        <MaterialIcons
        name={icon}
        size={23}
        color={checked ? Colors.PRIMARY_COLOR : Colors.TEXT_COLOR_2}
        />
        </TouchableOpacity>
      </View>
      <Text style={{ color: Colors.ERROR_COLOR, fontSize: Screen.width(3), fontWeight: '300' }}>
          {`You have ${reward} vnd`}
      </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  
});
export default Reward;