import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View, TouchableOpacity
} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import EvilIcons from 'react-native-vector-icons/EvilIcons'; // map-marker-outline
import Screen from '../../utils/screen';
import Colors from '../../contants/colors';
import Strings from '../../contants/strings';
import TextBox from '../../components/TextBox';


class Location extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  componentDidMount() {

  }

  render() {
    const { onPressEditLocation, location, isEdit, onChangeText } = this.props;
    return (

      <View style={{ flexDirection: 'column' }}>
        <Text style={{ fontSize: 15, color: Colors.TEXT_COLOR }}>
          {Strings.order_delivery_location}
        </Text>
        {isEdit
          ? (
            <TextBox
              icon="map-marker"
              placeholder={'Another address'}
              onChangeText={onChangeText}
              selectionColor={Colors.TEXT_COLOR}
              placeholderTextColor={Colors.PRIMARY_COLOR}
              styleTextBox={{ fontSize: 13 }}
            />
          ) : (
            <TouchableOpacity
              style={{ flexDirection: 'row', justifyContent: 'space-between' }}
              onPress={onPressEditLocation}
            >
              <View style={{ flexDirection: 'row' }}>
                <EvilIcons
                  name="location"
                  color={Colors.ERROR_COLOR}
                  size={23}
                />
                <Text style={{ marginHorizontal: 5, fontSize: 13, color: Colors.PRIMARY_COLOR }}>
                  {location}
                </Text>
              </View>
              <AntDesign
                name="edit"
                size={20}
                color={Colors.PRIMARY_COLOR}
              />
            </TouchableOpacity>
          )}

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  }
});
export default Location;