import React, { Component } from 'react';
import {
  Image,
  StyleSheet,
  View
} from 'react-native';
import { connect } from 'react-redux';
import Colors from '../../contants/colors';
import { requsetLogin } from '../../redux/actions/loginActions';
import { requestGetUserInfo } from '../../redux/actions/getUserInfoActions';
import Storage from '../../utils/storage';
import Container from '../../components/Container';

class SplashScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true
    }
  };

  componentDidMount() {
    this._.showLoading();
    const { navigation } = this.props;
    Storage.getData('phone', (data) => {
      if (data) {
        this.autoLogin(data.phone);
        // navigation.navigate('TabBarNavigator');
      } else {
        navigation.navigate('LogInScreen');
      }
    });
    this._.hideLoading();
  }

  componentWillReceiveProps(nextProps) {
    const { login } = nextProps;
    const { navigation } = this.props;
    if (login.logged) {
      navigation.navigate('TabBarNavigator');
    } else {
      navigation.navigate('LogInScreen');
    }
  }

  autoLogin = (param) => {
    const { dispatchLogin } = this.props;
    dispatchLogin({ phone: param });
  }

  render() {
    return (
      <Container
        ref={component => this._ = component}
      >
        <View style={styles.opacityView}
        />
        <Image
          resizeMode='contain'
          source={require('../../assets/images/splash.jpg')}
          style={styles.opacityImg}
        />
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  opacityView: {
    backgroundColor: Colors.SECOND_COLOR,
    position: 'absolute',
    top: 0,
    right: 0,
    left: 0,
    bottom: 0,
    opacity: 0.8
  },
  opacityImg: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    opacity: 0.5
  }
});

const mapStateToProps = state => ({
  login: state.login,
  getUserInfo: state.getUserInfo
});

const mapDispatchToProps = dispatch => ({
  dispatchGetUserInfo: (data) => dispatch(requestGetUserInfo(data)),
  dispatchLogin: (data) => dispatch(requsetLogin(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SplashScreen);