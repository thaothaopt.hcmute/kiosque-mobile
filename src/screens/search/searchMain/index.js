import React, { Component } from 'react';
import {
  StyleSheet,
  Text, TouchableOpacity,
  View
} from 'react-native';
import { Tab, Tabs } from 'native-base';
import { connect } from 'react-redux';
import SearchEngine from '../../../components/searchEngine';
import Container from '../../../components/Container';
import BottomCart from '../../../components/BottomCart';
import Strings from '../../../contants/strings';
import Storage from '../../../utils/storage';
import BestSellerTab from '../bestSellerTab';
import HealthyTab from '../healthyTab';
import Colors from '../../../contants/colors';
import Screen from '../../../utils/screen';
import { requestInitCart } from '../../../redux/actions/requestCartActions';

class SearchMainScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text: '',
      hideResults: false
    };
  }

  componentDidMount() {
    const { dispatchInitCart } = this.props;
    Storage.getData('cart', (data) => {
      if (data) {
        dispatchInitCart(data);
      } else {
        return [];
      }
    });
  }

  render() {
    const { navigation, requestCart } = this.props;

    return (
      <Container
        bottomComponent={(
          <BottomCart
            navigation={navigation}
          />
        )}
        ref={component => this._ = component}
      >
        <SearchEngine
          navigation={navigation}
        />
        <Tabs
          tabBarUnderlineStyle={{ backgroundColor: Colors.BACKGROUND_COLOR, height: 3 }}
        >
          <Tab
            heading={Strings.search_tab_bestseller_drink}
            tabStyle={styles.tabStyle}
            activeTabStyle={styles.tabStyle}
            textStyle={styles.textStyle}
            activeTextStyle={[{ fontWeight: 'bold' }, styles.textStyle]}
          >
            <BestSellerTab/>
          </Tab>
          <Tab
            heading={Strings.search_tab_healthy_drink}
            activeTabStyle={styles.tabStyle}
            tabStyle={styles.tabStyle}
            textStyle={styles.textStyle}
            activeTextStyle={[{ fontWeight: 'bold' }, styles.textStyle]}
          >
            <HealthyTab/>
          </Tab>
        </Tabs>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  }, tabStyle: {
    backgroundColor: Colors.PRIMARY_COLOR
  },
  textStyle: {
    fontSize: Screen.width(4),
    color: Colors.TEXT_TABBAR_TITLE
  },
});

const mapStateToProps = state => ({
  getProducts: state.getProducts,
  requestCart: state.requestCart
});

const mapDispatchToProps = dispatch => ({
  dispatchInitCart: data => dispatch(requestInitCart(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(SearchMainScreen);