import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View
} from 'react-native';


class Sample extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }


  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          {'Sample'}
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  
});
export default Sample;