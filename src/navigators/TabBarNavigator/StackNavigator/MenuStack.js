import { StackNavigator } from 'react-navigation';
//import MenuMainScreen from '../../../screens/menu/menuMain';
import MainBrand from '../../../screens/menu/brand';
// import DrinkListScreen from '../../../screens/menu/drinkList';
import DetailProductScreen from '../../../screens/menu/detailProduct'

export const MenuStack = StackNavigator(
    {
        MenuStack: {
            //screen : MenuMainScreen
            screen : MainBrand
        },
        DetailProductScreen:{
            screen: DetailProductScreen
        }
    },
    {
        headerMode: 'none',
        mode: 'default',
        orientation : 'portrait',
    }
);
