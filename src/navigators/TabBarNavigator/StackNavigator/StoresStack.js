import { StackNavigator } from 'react-navigation';
import StoresMainScreen from '../../../screens/stores/storesMain';


export const StoresStack = StackNavigator(
    {
        StoresStack: {
            screen : StoresMainScreen
        },
    },
    {
        headerMode: 'none',
        mode: 'default',
        orientation : 'portrait'
    }
);
