import { StackNavigator } from 'react-navigation';
import SearchMainScreen from '../../../screens/search/searchMain';


export const SearchStack = StackNavigator(
    {
        SearchStack: {
            screen : SearchMainScreen
        },
    },
    {
        headerMode: 'none',
        mode: 'default',
        orientation : 'portrait'
    }
);
