import { StackNavigator } from 'react-navigation';
import ProfileScreen from '../../../screens/profile/profileMain';
// import EditProfileScreen from '../../../screens/profile/editProfile';
import HistoryScreen from '../../../screens/profile/history';
import RewardsScreen from '../../../screens/profile/rewards';

export const ProfileStack = StackNavigator(
    {
        ProfileStack: {
            screen : ProfileScreen
        },
        // EditProfileScreen: {
        //     screen: EditProfileScreen
        // },
        HistoryScreen: {
            screen: HistoryScreen
        },
        RewardsScreen: {
            screen: RewardsScreen
        }
    },
    {
        headerMode: 'none',
        mode: 'default',
        orientation : 'portrait'
    }
);
