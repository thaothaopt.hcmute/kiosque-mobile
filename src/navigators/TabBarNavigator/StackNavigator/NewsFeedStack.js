import { StackNavigator } from 'react-navigation';
import NewsFeedMainScreen from '../../../screens/newsFeed/newsFeedMain';
import DetailNewsScreen from '../../../screens/newsFeed/detailNews';
import NotificationScreen from '../../../screens/newsFeed/notifycation';

export const NewsFeedStack = StackNavigator(
    {
        NewsFeedStack: {
            screen : NewsFeedMainScreen
        },
        DetailNewsScreen: {
            screen: DetailNewsScreen
        },
        NotificationScreen: {
            screen: NotificationScreen
        }
    },
    {
        headerMode: 'none',
        mode: 'default',
        orientation : 'portrait'
    }
);
