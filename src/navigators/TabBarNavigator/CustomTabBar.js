import React, { Component } from 'react';
import { View, TouchableOpacity, Text, Image,
    AsyncStorage, StatusBar, Platform,
    StyleSheet } from 'react-native';
import { NavigationActions, StackActions  } from 'react-navigation';
import Colors from '../../contants/colors';
import Icons from 'react-native-vector-icons/Entypo';
import Screen from '../../utils/screen';

const resetActionMenu = StackActions .reset({
    index: 0,
    actions: [NavigationActions.navigate({ routeName: 'MenuStack' })],
});
const resetActionNewsFeed = StackActions.reset({
    index: 0,
    actions: [NavigationActions.navigate({ routeName: 'NewsFeedStack' })]
});
const resetActionProfile = StackActions.reset({
    index: 0,
    actions: [NavigationActions.navigate({ routeName: 'ProfileStack' })]
});
const resetActionStores = StackActions.reset({
    index: 0,
    actions: [NavigationActions.navigate({ routeName: 'StoresStack' })]
});
const resetActionSearch = StackActions.reset({
    index: 0,
    actions: [NavigationActions.navigate({ routeName: 'SearchStack' })]
});

class CustomTabBar extends Component {

    constructor(props) {
        super(props);
        this.state = {
            logged: '',
            screen: 'NewsFeedStack',
        };
    }

    async getKey() {
        try {
            const value = await AsyncStorage.getItem('@MySuperStore:screen');
            if (!value) {
                this.setState({ screen: value });
            }
        } catch (error) {
            //
        }
    }

    async saveKey(value) {
        try {
            await AsyncStorage.setItem('@MySuperStore:screen', value);
            this.setState({
                screen: value
            });
        } catch (error) {
            //
        }
    }

    async resetKey() {
        try {
            await AsyncStorage.removeItem('@MySuperStore:screen');
            const value = await AsyncStorage.getItem('@MySuperStore:screen');
            this.setState({ screen: value });
        } catch (error) {
            //
        }
    }


    lightStatusbar() {
        if (Platform.OS === 'android') {
            StatusBar.setBackgroundColor(Colors.PRIMARY_COLOR);
        }
        StatusBar.setBarStyle('light-content');
    }

    darkStatusbar() {
        if (Platform.OS === 'android') {
            StatusBar.setBackgroundColor(Colors.PRIMARY_COLOR);
        }
        StatusBar.setBarStyle('light-content');
    }

    componentWillReceiveProps(nextProps) {
        const index = nextProps.navigation.state.index;
        const rootNode = nextProps.navigation.state.routes[index];
        const childIndex = rootNode.index;
        const childNode = rootNode.routes[childIndex];
        const screenName = childNode.routeName;
        // if(this.props.usergainsReducer != nextProps.usergainsReducer && nextProps.usergainsReducer.usergains.data != null) {
        //     this.setState({
        //         headerWallet:{
        //             ...this.state.headerWallet,
        //             [rootNode.key] : true
        //         }
        //     });
        // }
        switch (screenName) {
            //dark content
            case 'MenuStack':
            case 'NewsFeedStack':
            case 'ProfileStack':
            case 'StoresStack':
            case 'SearchStack':
                this.lightStatusbar();
                break;
            //light content
            default:
                this.darkStatusbar();
                break;
        }

    }

    navigateScreenAndUpdateStatusBar(routeName, routes ) {
        if (routeName == 'NewsFeedStack' && this.state.screen != 'NewsFeedStack') {
            this.saveKey('NewsFeedStack');
            this.props.navigation.navigate(routeName);
            this.props.navigation.dispatch(resetActionNewsFeed);
        } else if (routeName == 'MenuStack' && this.state.screen != 'MenuStack') {
            this.saveKey('MenuStack');
            this.props.navigation.navigate(routeName);
            this.props.navigation.dispatch(resetActionMenu);
        } else if (routeName == 'ProfileStack' && this.state.screen != 'ProfileStack') {
            this.saveKey('ProfileStack');
            this.props.navigation.navigate(routeName);
            this.props.navigation.dispatch(resetActionProfile);
        } else if (routeName == 'StoresStack' && this.state.screen != 'StoresStack') {
            this.saveKey('StoresStack');
            this.props.navigation.navigate(routeName);
            this.props.navigation.dispatch(resetActionStores);
        } else if (routeName == 'SearchStack' && this.state.screen != 'SearchStack') {
            this.saveKey('SearchStack');
            this.props.navigation.navigate(routeName);
            this.props.navigation.dispatch(resetActionSearch);
        }
    }

    render() {
        let navigation = this.props.navigation;
        let icons = ['news', 'menu', 'magnifying-glass', 'home', 'user'];

        let titles = ['News', 'Menu', 'Search', 'Stores', 'Profile'];

        const { routes, index } = navigation.state;
        return (
            <View style={styles.tabContainer}>
                {routes.map((route, idx) => {
                    let isActive = index == idx;
                    return (
                        <TouchableOpacity
                            key={idx}
                            onPress={() => {
                                this.navigateScreenAndUpdateStatusBar(route.routeName, route);
                            }}
                            style={styles.tab}>
                            <Icons
                            name={icons[idx]}
                            size={20}
                            color={isActive ? Colors.PRIMARY_COLOR : Colors.SECOND_COLOR}
                            style={{marginTop: 2}}
                            />
                            <Text
                                style={[
                                    {
                                        color: isActive ? Colors.PRIMARY_COLOR : Colors.SECOND_COLOR,
                                        textAlign: 'center', fontSize: Screen.width(3.5)
                                    }
                                ]}>
                                {titles[idx]}
                            </Text>
                        </TouchableOpacity>
                    );
                })}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    tabContainer: {
        flexDirection: 'row',
        height: 50,
        backgroundColor: Colors.BACKGROUND_COLOR,
        borderTopColor: Colors.SECOND_COLOR,
        borderTopWidth: 0.5
    },
    tab: {

        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-start',
        padding: 5
    },
    tabLongText: {
        flex: 1.3,
        alignItems: 'center',
        justifyContent: 'flex-start',
        padding: 13
    },
});

export default (CustomTabBar);
