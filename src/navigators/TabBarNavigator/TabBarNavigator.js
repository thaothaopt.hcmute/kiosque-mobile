import { TabNavigator } from 'react-navigation';

import {NewsFeedStack} from './StackNavigator/NewsFeedStack';
import {ProfileStack} from './StackNavigator/ProfileStack';
import {MenuStack} from './StackNavigator/MenuStack';
import {StoresStack} from './StackNavigator/StoresStack';
import {SearchStack} from './StackNavigator/SearchStack';

import CustomTabBar from './CustomTabBar';

const TabBarNavigator = TabNavigator({
    NewsFeedStack: {
        screen: NewsFeedStack
    },
    MenuStack: {
        screen: MenuStack
    },
    SearchStack: {
        screen: SearchStack
    },
    StoresStack: {
        screen: StoresStack
    },
    ProfileStack: {
        screen: ProfileStack
    }
  },
  {
    headerMode: 'none',
    tabBarPosition: 'bottom',
    animationEnabled: false,
    swipeEnabled: false,
    initialRouteName: 'NewsFeedStack',
    tabBarComponent: CustomTabBar,
    lazy: true,
}
  );
  
  export default (TabBarNavigator);