import {
    reduxifyNavigator,
    createReactNavigationReduxMiddleware,
  } from 'react-navigation-redux-helpers';
import { connect } from 'react-redux';
import { createStackNavigator } from 'react-navigation';
import LogInScreen from '../screens/logIn';
import SplashScreen from '../screens/splash';
import VerifyScreen from '../screens/verify';
import EditProfileScreen from '../screens/profile/editProfile';
import UpdatePhoneScreen from '../screens/updatePhone';
import DetailProductScreen from '../screens/menu/detailProduct';
import LastStepOrderScreen from '../screens/lastStepOrder';
import OrderCompleteScreen from '../screens/lastStepOrder/OrderComplete';
import DrinkListScreen from '../screens/menu/drinkList';
import TabBarNavigator from './TabBarNavigator/TabBarNavigator';
import MenuMainScreen from '../screens/menu/menuMain';
import DrinkBrand from '../screens/menu/drinkBrand';

const RootNavMiddleware = createReactNavigationReduxMiddleware(
    'root',
    state => state.nav
  );
  
const RootNavigator = createStackNavigator(
    {
        SplashScreen: { screen: SplashScreen},
        LogInScreen: {screen:  LogInScreen},
        VerifyScreen: {screen: VerifyScreen},
        EditProfileScreen: {screen: EditProfileScreen},
        UpdatePhoneScreen: {screen: UpdatePhoneScreen},
        DetailProductScreen: {screen: DetailProductScreen},
        LastStepOrderScreen: {screen: LastStepOrderScreen},
        OrderCompleteScreen: {screen: OrderCompleteScreen},
        DrinkListScreen: {screen: DrinkListScreen},
        TabBarNavigator: { screen: TabBarNavigator },
        MenuMainScreen: {screen: MenuMainScreen},
        DrinkBrand: {screen: DrinkBrand},
    },
    {
        headerMode: 'none',
        mode: 'modal',
        initialRouteName: 'SplashScreen',
    }
);

const mapStateToProps = state => ({
    state: state.navigation
  });
const RootWithNavigationState = reduxifyNavigator(RootNavigator, 'root');
const RootNavigation = connect(mapStateToProps)(RootWithNavigationState);

export { RootNavigation, RootNavMiddleware, RootNavigator };