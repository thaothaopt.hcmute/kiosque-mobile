import { AsyncStorage } from 'react-native';

class Storage {

    static setData(key, value) {
        AsyncStorage.setItem(key, JSON.stringify(value), (err) => {
           
        });
    }
    static getData(key, callback) {
        AsyncStorage.getItem(key, (err, data) => {
            if (data) {
                callback(JSON.parse(data));
            } else {
                callback(null);

            }
        });
    }

    
    static removeData(key) {
        AsyncStorage.removeItem(key);
    }

}

export default Storage;