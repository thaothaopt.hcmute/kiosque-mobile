import Colors from '../contants/colors';

class Dialog {
  static createConfirms(title, content, decline, accept, callback) {
    return {
      title,
      content,
      ok: {
        text: accept,
        style: {
          color: Colors.PRIMARY_COLOR
        },
        callback: () => {
          callback(true);
        },
      },
      cancel: {
        text: decline,
        style: {
          color: Colors.PRIMARY_COLOR
        },
        callback: () => {
          callback(false);
        },
      },
    };
  }


  static createMessage(title, content, button, callback) {
    return {
      title,
      content,
      btn: {
        style: {
          color: Colors.PRIMARY_COLOR
        },
        text: button,
        callback,
      },
    };
  }
}
export default Dialog;
