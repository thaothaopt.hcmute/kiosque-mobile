import { createStore, applyMiddleware, compose } from 'redux';
import { createLogger } from 'redux-logger';
import createSagaMiddleware from 'redux-saga';
import rootReducer from '../redux/reducers';
import rootSaga from '../redux/sagas';
import { RootNavMiddleware } from '../navigators/RootNavigator';

const sagaMiddleware = createSagaMiddleware();

export default function configureStore() {
  const middlewares = [];
  const enhancers = [];
  // logger
  if (__DEV__) {
    const loggerMiddleware = createLogger({
      collapsed: (getState, action, logEntry) => !logEntry.error,
    });
    middlewares.push(loggerMiddleware);
  }
  middlewares.push(sagaMiddleware);
  middlewares.push(RootNavMiddleware);

  enhancers.push(applyMiddleware(...middlewares));

  const store = createStore(rootReducer, compose(...enhancers));
  sagaMiddleware.run(rootSaga);
  return store;
}
