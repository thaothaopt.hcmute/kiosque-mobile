import { Dimensions } from 'react-native';

const screen = Dimensions.get('window');

class Screen {
  static width(percent) {
    return (percent / 100) * screen.width;
  }

  static height(percent) {
    return (percent / 100) * screen.height;
  }
}

export default Screen;
