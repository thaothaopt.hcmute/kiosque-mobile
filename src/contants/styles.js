import { StyleSheet } from 'react-native';
import Colors from './colors';
import Screen from '../utils/screen';

export const rootStyles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    backgroundColor: Colors.BACKGROUND_COLOR,
  },
  body: {
    flex: 1,
    width: '100%',
    backgroundColor: Colors.BACKGROUND_COLOR
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  absolute: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0
  },
  row: {
    flexDirection: 'row'
  },
  row_between: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  default_size: {
    fontSize: 15
  },
  large_size: {
    fontSize: 17
  },
  small_size: {
    fontSize: 14
  },
  s_small_size: {
    fontSize: 13
  },
  default_text: {color: Colors.TEXT_COLOR_2, fontSize: Screen.width(3.5), padding: Screen.width(1)},
  avatar: { width: 40, height: 40, borderRadius: 20 },
  avatar_large: { width: 60, height: 60, borderRadius: 30 }
});
