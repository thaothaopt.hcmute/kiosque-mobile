export default {
    PRIMARY_COLOR: '#36b006',

    SECOND_COLOR: '#aae18f',

    TEXT_COLOR: '#444444',

    TEXT_COLOR_2: '#999999',

    TEXT_TABBAR_TITLE: '#ffffff',

    TRANSPARENT: 'transparent',

    BACKGROUND_COLOR: 'white',

    SECOND_BACKGROUND_COLOR: '#e1f2d0',

    DISABLE_COLOR: '#EEEEEE',

    ERROR_COLOR: 'red',

    STAR_COLOR: '#FFCC00',

    HEART_COLOR: '#c10d16'
};