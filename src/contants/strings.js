export default {
  /**SIGN IN */
  LOGIN: 'Log In',
  YOURREWARDS: 'Your money until now is ', 
  NOREWARDS: 'Log in to get your rewards',
  CONTINUE: 'Continue',
  EARNPOINTS: 'Login to earn points',
  UPDATEPHONE: 'Update phone number',
  NEWPHONE: 'Enter your new phone number!',
  INVALIDPHONE: 'Please enter valid phone number',

  /**Verify phone */
  VERIFYPHONE: 'Verify phone',

  /**NEWSFEED */
  SALES: 'PROMOTIONS',
  FRESH: 'EVENTS',

  /**MENU */
  CATEGORY: 'Category',
  menu_all_drinks_title: 'Our Drinks',
  menu_brand_title: 'Brands',

  /** Search */
  search_tab_bestseller_drink: 'Best Seller Drinks',
  search_tab_healthy_drink: 'Healthy Drinks',

  /**TABBARNAVIGATOR */
  PROFILE: 'Profile',

  /**HISTORY */
  HISTORY: 'History',
  DONEORDERS: 'Done Orders',
  ANOTHER: 'Another',
  STATUS: 'Status',
  success: 'Successful delivery',
  pending: 'On delivery',
  failed: 'Failed delivery',

  /**REWARDS */
  REWARDS: 'Your rewards until now is ',
  _reward: 'Rewards',

  /**EMPTY */
  RECONNECT: 'Something wrong!\n Please reconnect!',

  /**DETAIL PRODUCT */
  SIZE: 'Size:',

  /** ORDER*/
  INFO_ORDER: 'Order\'s Information',
  INFO_RECIPIENT: 'Recipient\'s Information',
  ADDRESS_RECIPIENT: 'Recipient\'s Address',
  INFO_DETAIL_ORDER: 'Order\'s Detail Information',
  HAVE_DISCOUNT_CODE: 'You have a discount code: ',
  TOTAL: 'Total',
  TYPE_DISCOUNT_CODE: 'Type your discount code',
  ORDER_FINISHED: 'The order is finished!',
  order_thank_for_order: 'Thank for your chosing us!',
  order_your_phone: 'Your phone',
  order_delivery_location: 'Delivery location',

  /** cart */
  YOUR_CART: 'Your cart',

  /** Profile */
  profile_rating_hint: 'Type your review',
  profile_rating_submit_button: 'Submit',
};