import { create } from 'apisauce';
import * as ServiceUtility from './serviceUtility';
import * as Configs from './config';

const API = create({
    baseURL: Configs.SERVER,
    timeout: 4000,
    headers: ServiceUtility.getHeaderApplicationJson()
});

API.addRequestTransform(transform => {

});

// Monitor API
API.addMonitor(response => {

});

API.addResponseTransform(response => {
    // TODO: success or fail
    // http status code
});

export const setHeaderToken = token => {
    if (token) {
        API.setHeader('Authorization', `Bearer ${token}`);
    }
};

export default API;