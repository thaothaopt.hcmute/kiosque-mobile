import API from './configAPI';
import FormData from 'form-data';

export const requestGetType = () => API.get('/types');

export const requestGetProducts = () => {
    return API.get(`/products`);
};

export const requestGetBrands = () => {
    return API.get(`/brands`);
};

export const requestGetProductsBrand = (param) => {
    return API.get(`/products/brand/${param}`);
}

export const requestGetTypesBrand = (param) => {
    return API.get(`types/brand/${param}`);
}

export const requestGetProductsType = (param) => {
    return API.get(`/products/type/${param}`);
}

export const requestGetNews = (param) => API.get(`/promotions/${param}`);

export const requestLogin = (param) => {
    return API.post('/auths/login', param);
}

export const requestGetUserInfo = (param) => API.get(`/users/auth/${param}`);

export const requestUpdatePhone = (param) => {
    return API.patch(`/auths/${param.authid}`, param.params);
}

export const requestUpdateProfile = (param) => {
    return API.patch(`/users/auth/${param.userid}`, param.params);
}

export const requestGetAddresses = (param) => API.get(`/addresses/${param}`);

export const AddFavouriteDrink = (params) => {
    return API.post(`/favouritedrinks/${params.authid}`, params);
}

export const DeleteFavouriteDrink = (param) => API.delete(`/favouritedrinks/${param}`);

export const requestOrder = (params) => API.post('/bills', params);

export const requestGetHistory = (param) => API.get(`/bills/authid/${param}`);

export const requestGetReward = (param) => API.get(`/rewards/${param}`);

export const requestCreateReward = (params) => API.post(`/rewards/${params.authid}`, params);

export const requestUpdateReward = (params) => API.patch(`/rewards/${params.authid}`, params.params);

export const requestPostReview = (params) => API.post('/reviews', params);

export const uploadImage = (params) => {
    const form = new FormData()
    form.append('image', {
      name: params.name,
      uri: params.pathToImageOnFilesystem,
      type: 'image/jpg'
    })
    const headers = {
      'Content-Type': 'multipart/form-data'
    }
    
    return API.patch(`/users/avatar/${params.userid}`, form, {headers})
  }
