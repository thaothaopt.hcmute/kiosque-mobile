/**
 * response {
 ** data - the object originally from the server that you might wanna mess with duration - the number of milliseconds
 ** problem - the problem code (see the bottom for the list)
 ** ok - true or false
 ** status - the HTTP status code
 ** headers - the HTTP response headers
 ** config - the underlying axios config for the request
 ** }
 * @param {*} response
 */

export const processResponseService = response => ({
    status: response.ok,
    data: response.data,
    message: response.status ? response.status : response.problem
});

// /**
//  * get form data from params
//  * @param {*} params
//  */
// export const getFormData = params => {
//     var formDatas = new FormData();
//     Object.keys(params).map(key => formDatas.append(key, encodeURIComponent(params[key])));
//     return formDatas;
// };

/**
 * get header for url encoded
 */
export const getHeaderFormUrlEncoded = () => ({
    // 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8
    //'Authorization': 'Basic cGxlYmljb206bWJpb25seTIwMTg='
});

/**
 * get header for application json
 */
export const getHeaderApplicationJson = (token) => ({
    'Content-Type': 'application/json',
    'Authorization': `Bearer ${token}`
});

export const getHeaderTextHtml = () => ({
    'Accept': 'text/html',
    'Content-Type': 'text/html',
    //'Authorization': 'Basic cGxlYmljb206bWJpb25seTIwMTg='
});
