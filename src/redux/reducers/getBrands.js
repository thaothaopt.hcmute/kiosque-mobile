import { GET_BRANDS } from '../actions/actionTypes';

const initState = {
  data: {},
  isFetching: false,
  isSuccess: false,
  error: {}
};

export default function Brands(state = initState, action) {
  switch (action.type) {
    case GET_BRANDS.REQUEST: {
      return {
        ...state,
        isFetching: true,
        isSuccess: false
      };
    }
    case GET_BRANDS.SUCCESS: {
      return {
        ...state,
        isFetching: false,
        isSuccess: true,
        data: action.data.brands
      };
    }
    case GET_BRANDS.ERROR: {
      return {
        ...state,
        isFetching: false,
        isSuccess: false,
        error: action.error
      };
    }
    default:
      return state;
  }
}
