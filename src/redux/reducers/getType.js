import { GET_TYPE } from '../actions/actionTypes';

const initState = {
  data: {},
  isFetching: false,
  isSuccess: false,
  error: {}
};

export default function Type(state = initState, action) {
  switch (action.type) {
    case GET_TYPE.REQUEST: {
      return {
        ...state,
        isFetching: true,
        isSuccess: false
      };
    }
    case GET_TYPE.SUCCESS: {
      return {
        ...state,
        isFetching: false,
        isSuccess: true,
        data: action.data.types
      };
    }
    case GET_TYPE.ERROR: {
      return {
        ...state,
        isFetching: false,
        isSuccess: false,
        error: action.error
      };
    }
    default:
      return state;
  }
}
