import { GET_REWARD } from '../actions/actionTypes';

const initState = {
  data: {},
  reward: 0,
  isFetching: false,
  isSuccess: false,
  error: {}
};

export default function Reward(state = initState, action) {
  switch (action.type) {
    case GET_REWARD.REQUEST: {
      return {
        ...state,
        isFetching: true,
        isSuccess: false
      };
    }
    case GET_REWARD.SUCCESS: {
      return {
        ...state,
        isFetching: false,
        isSuccess: true,
        data: action.data.reward,
        reward: action.data.reward[0].reward
      };
    }
    case GET_REWARD.ERROR: {
      return {
        ...state,
        isFetching: false,
        isSuccess: false,
        error: action.error
      };
    }
    default:
      return state;
  }
}
