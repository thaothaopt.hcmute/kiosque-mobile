import { GET_NEWS_DAILY } from '../actions/actionTypes';

const initState = {
  data: [],
  isFetching: false,
  isSuccess: false,
  error: {}
};

export default function News(state = initState, action) {
  switch (action.type) {
    case GET_NEWS_DAILY.REQUEST: {
      return {
        ...state,
        isFetching: true,
        isSuccess: false
      };
    }
    case GET_NEWS_DAILY.SUCCESS: {
      return {
        ...state,
        isFetching: false,
        isSuccess: true,
        data: action.data.promotions
      };
    }
    case GET_NEWS_DAILY.ERROR: {
      return {
        ...state,
        isFetching: false,
        isSuccess: false,
        error: action.error
      };
    }
    default:
      return state;
  }
}
