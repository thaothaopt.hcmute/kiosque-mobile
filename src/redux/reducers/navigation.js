import { NavigationActions } from 'react-navigation';
import { RootNavigator } from '../../navigators/RootNavigator';
import { Keyboard } from 'react-native';

const initState = RootNavigator.router.getStateForAction(NavigationActions.init());

export default (state = initState, action) => {
    const nextState = RootNavigator.router.getStateForAction(action, state);
    if ( action.type === 'Navigation/NAVIGATE') {
        Keyboard.dismiss();   
        try {
            if (action.routeName === state.routes[0].routes[state.routes[0].index].routeName) {
                return state;
            }
        } catch(ex) {
            //
        }
    }
    return nextState || state;
};
