import { combineReducers } from 'redux';
import navigation from './navigation';
import login from './login';
import getType from './getType';
import getNewsDaily from './getNewsDaily';
import getUserInfo from './getUserInfo';
import getNewsEvent from './getNewsEvent';
import getProducts from './getProducts';
import getProductsType from './getProductsType';
import requestCart from './requestCart';
import getAddresses from './getAddresses';
import getBrands from './getBrands';
import getProductsBrand from './getProductsBrand';
import getTypesBrand from './getTypesBrand';
import getHistory from './getHistory';
import getReward from './getReward';

const rootReducer = combineReducers({
    navigation,
    login,
    getType,
    getNewsDaily,
    getUserInfo,
    getNewsEvent,
    getProducts,
    getProductsType,
    requestCart,
    getAddresses,
    getBrands,
    getProductsBrand,
    getTypesBrand,
    getHistory,
    getReward
});

export default rootReducer;
