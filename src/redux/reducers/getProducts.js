import { GET_PRODUCTS } from '../actions/actionTypes';

const initState = {
  data: {},
  isFetching: false,
  isSuccess: false,
  error: {}
};

export default function Products(state = initState, action) {
  switch (action.type) {
    case GET_PRODUCTS.REQUEST: {
      return {
        ...state,
        isFetching: true,
        isSuccess: false
      };
    }
    case GET_PRODUCTS.SUCCESS: {
      return {
        ...state,
        isFetching: false,
        isSuccess: true,
        data: action.data.products
      };
    }
    case GET_PRODUCTS.ERROR: {
      return {
        ...state,
        isFetching: false,
        isSuccess: false,
        error: action.error
      };
    }
    default:
      return state;
  }
}
