import { GET_ADDRESS } from '../actions/actionTypes';

const initState = {
  data: {},
  isFetching: false,
  isSuccess: false,
  error: {}
};

export default function Addresses(state = initState, action) {
  switch (action.type) {
    case GET_ADDRESS.REQUEST: {
      return {
        ...state,
        isFetching: true,
        isSuccess: false
      };
    }
    case GET_ADDRESS.SUCCESS: {
      return {
        ...state,
        isFetching: false,
        isSuccess: true,
        data: action.data.addresses
      };
    }
    case GET_ADDRESS.ERROR: {
      return {
        ...state,
        isFetching: false,
        isSuccess: false,
        error: action.error
      };
    }
    default:
      return state;
  }
}
