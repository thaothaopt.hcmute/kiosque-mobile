import { LOGIN } from '../actions/actionTypes';
import { setHeaderToken } from '../service/configAPI';
import Storage from '../../utils/storage';
import { getHeaderApplicationJson } from '../service/serviceUtility';

const initState = {
  message: {},
  signedup: {},
  data: {},
  isFetching: false,
  isSuccess: false,
  error: {},
  auth: {},
  logged: false
};

export default function Login(state = initState, action) {
  switch (action.type) {
    case LOGIN.REQUEST_ACCESS_TOKEN: {
        return {
          ...state,
          isFetching: true,
          isSuccess: false,
          logged: false
        };
      }
      case LOGIN.SUCCESS_ACCESS_TOKEN: {
        return {
          ...state,
          isFetching: false,
          isSuccess: true,
          auth: action.data,
          // logged: true
        };
      }
      case LOGIN.REQUEST: {
        return {
          ...state,
          isFetching: true,
          isSuccess: false,
          logged: false
        };
      }
    case LOGIN.SUCCESS: {
      if (action.data.status) {
        Storage.setData('phone', action.data.auth);
        Storage.setData('token', action.data.token);
        setHeaderToken(action.data.token);
        getHeaderApplicationJson(action.data.token);
      }
      
      return {
        ...state,
        isFetching: false,
        isSuccess: true,
        message: action.data.message,
        signedup: action.data.signedup,
        auth: action.data.auth,
        logged: (action.data && action.data.status && action.data.status) ? action.data.status : false
      };
    }
    case LOGIN.ERROR: {
      return {
        ...state,
        isFetching: false,
        isSuccess: false,
        error: action.error,
        logged: false
      };
    }
    default:
      return state;
  }
}
