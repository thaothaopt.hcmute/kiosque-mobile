import { GET_PRODUCTS_BRAND } from '../actions/actionTypes';

const initState = {
  data: {},
  isFetching: false,
  isSuccess: false,
  error: {}
};

export default function ProductsBrand(state = initState, action) {
  switch (action.type) {
    case GET_PRODUCTS_BRAND.REQUEST: {
      return {
        ...state,
        isFetching: true,
        isSuccess: false
      };
    }
    case GET_PRODUCTS_BRAND.SUCCESS: {
      return {
        ...state,
        isFetching: false,
        isSuccess: true,
        data: action.data
      };
    }
    case GET_PRODUCTS_BRAND.ERROR: {
      return {
        ...state,
        isFetching: false,
        isSuccess: false,
        error: action.error
      };
    }
    default:
      return state;
  }
}
