import { REQUEST_CART } from '../actions/actionTypes';
import Storage from '../../utils/storage';


const _cart = () => {
  let newList = [];
  Storage.getData('cart', (data) => {
    if (data) {
      newList = data;
    } else {
      return [];
    }
  });
  return newList;
}
const initState = {
  selected: false,
  cart: [],
  cleaned: false
};

export default (state = initState, action) => {
  // eslint-disable-next-line no-param-reassign
  state.actionType = action.type;
  switch (action.type) {
    case REQUEST_CART.INIT_CART: {
      return {
        ...state,
        cart: action.data
      };
    }
    case REQUEST_CART.GET_CART: {
      return {
        ...state,
        cart: action.data
      };
    }
    case REQUEST_CART.UPDATE_CART: {
      return {
        ...state,
        selected: true,
        cart: (state.cart.length === action.data.length) ? action.data : [...state.cart, cart = action.data],
      };
    }
    case REQUEST_CART.CLEAN_CART: {
      return {
        ...state,
        cart: [],
        selected: false,
        cleaned: true
      };
    }
    default:
      return state;
  }
};
