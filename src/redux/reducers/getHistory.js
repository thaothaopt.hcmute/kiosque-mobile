import { GET_HISTORY } from '../actions/actionTypes';

const initState = {
  data: [],
  isFetching: false,
  isSuccess: false,
  error: {}
};

export default function History(state = initState, action) {
  switch (action.type) {
    case GET_HISTORY.REQUEST: {
      return {
        ...state,
        isFetching: true,
        isSuccess: false
      };
    }
    case GET_HISTORY.SUCCESS: {
      return {
        ...state,
        isFetching: false,
        isSuccess: true,
        data: action.data.bill
      };
    }
    case GET_HISTORY.ERROR: {
      return {
        ...state,
        isFetching: false,
        isSuccess: false,
        error: action.error
      };
    }
    default:
      return state;
  }
}
