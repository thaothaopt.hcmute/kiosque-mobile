import { GET_USERINFO } from '../actions/actionTypes';

const initState = {
  userinfo: {},
  isFetching: false,
  isSuccess: false,
  error: {}
};

export default function UserInfo(state = initState, action) {
  switch (action.type) {
    case GET_USERINFO.REQUEST: {
      return {
        ...state,
        isFetching: true,
        isSuccess: false
      };
    }
    case GET_USERINFO.SUCCESS: {
      return {
        ...state,
        isFetching: false,
        isSuccess: true,
        userinfo: action.data.userinfo
      };
    }
    case GET_USERINFO.ERROR: {
      return {
        ...state,
        isFetching: false,
        isSuccess: false,
        error: action.error
      };
    }
    default:
      return state;
  }
}
