import { REQUEST_CART } from './actionTypes';

export const requestGetCart = () => ({
  type: REQUEST_CART.GET_CART,
});

export const requestInitCart = (data) => ({
  type: REQUEST_CART.INIT_CART,
  data
});

export const requestUpdateCart = data => ({
  type: REQUEST_CART.UPDATE_CART,
  data
});

export const requestCleanCart = () => ({
  type: REQUEST_CART.CLEAN_CART,
});
