import { GET_PRODUCTS_BRAND } from './actionTypes';

export const requestGetProductsBrand = (data) => ({
    type: GET_PRODUCTS_BRAND.REQUEST,
    data
});

export const requestGetProductsBrandSuccess = data => ({
    type: GET_PRODUCTS_BRAND.SUCCESS,
  data
});

export const requestGetProductsBrandError = error => ({
  type: GET_PRODUCTS_BRAND.ERROR,
  error
});
