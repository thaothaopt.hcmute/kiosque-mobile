import { GET_USERINFO } from './actionTypes';

export const requestGetUserInfo = data => ({
  type: GET_USERINFO.REQUEST,
  data
});

export const requestGetUserInfoSuccess = data => ({
  type: GET_USERINFO.SUCCESS,
  data
});

export const requestGetUserInfoError = error => ({
  type: GET_USERINFO.ERROR,
  error
});
