import { GET_PRODUCTS } from './actionTypes';

export const requestGetProducts = (data) => ({
    type: GET_PRODUCTS.REQUEST,
    data
});

export const requestGetProductsSuccess = data => ({
    type: GET_PRODUCTS.SUCCESS,
  data
});

export const requestGetProductsError = error => ({
  type: GET_PRODUCTS.ERROR,
  error
});
