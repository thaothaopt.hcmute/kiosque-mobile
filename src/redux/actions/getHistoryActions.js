import { GET_HISTORY } from './actionTypes';

export const requestGetHistory = (data) => ({
  type: GET_HISTORY.REQUEST,
  data
});

export const requestGetHistorySuccess = data => ({
  type: GET_HISTORY.SUCCESS,
  data
});

export const requestGetHistoryError = error => ({
  type: GET_HISTORY.ERROR,
  error
});
