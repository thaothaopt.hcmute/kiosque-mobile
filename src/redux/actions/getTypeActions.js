import { GET_TYPE } from './actionTypes';

export const requestGetType = () => ({
  type: GET_TYPE.REQUEST
});

export const requestGetTypeSuccess = data => ({
  type: GET_TYPE.SUCCESS,
  data
});

export const requestGetTypeError = error => ({
  type: GET_TYPE.ERROR,
  error
});
