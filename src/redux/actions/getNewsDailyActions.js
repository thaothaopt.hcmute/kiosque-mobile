import { GET_NEWS_DAILY } from './actionTypes';

export const requestGetNewsDaily = (data) => ({
  type: GET_NEWS_DAILY.REQUEST,
  data
});

export const requestGetNewsDailySuccess = data => ({
  type: GET_NEWS_DAILY.SUCCESS,
  data
});

export const requestGetNewsDailyError = error => ({
  type: GET_NEWS_DAILY.ERROR,
  error
});
