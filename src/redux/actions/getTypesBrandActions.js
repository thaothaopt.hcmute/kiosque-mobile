import { GET_TYPES_BRAND } from './actionTypes';

export const requestGetTypesBrand = (data) => ({
    type: GET_TYPES_BRAND.REQUEST,
    data
});

export const requestGetTypesBrandSuccess = data => ({
    type: GET_TYPES_BRAND.SUCCESS,
  data
});

export const requestGetTypesBrandError = error => ({
  type: GET_TYPES_BRAND.ERROR,
  error
});
