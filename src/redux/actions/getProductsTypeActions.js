import { GET_PRODUCTS_TYPE } from './actionTypes';

export const requestGetProductsType = (data) => ({
    type: GET_PRODUCTS_TYPE.REQUEST,
    data
});

export const requestGetProductsTypeSuccess = data => ({
    type: GET_PRODUCTS_TYPE.SUCCESS,
  data
});

export const requestGetProductsTypeError = error => ({
  type: GET_PRODUCTS_TYPE.ERROR,
  error
});
