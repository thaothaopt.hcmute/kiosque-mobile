import { LOGIN } from './actionTypes';

export const requestAccessToken = data => ({
    type: LOGIN.REQUEST_ACCESS_TOKEN,
    data
  });
  
  export const requestAppTokenSuccess = data => ({
    type: LOGIN.SUCCESS_ACCESS_TOKEN,
    data
  });

export const requsetLogin = data => ({
    type: LOGIN.REQUEST,
    data
  });

  export const requestLoginSuccess = data => ({
    type: LOGIN.SUCCESS,
    data
  });
  
  export const requestLoginError = error => ({
    type: LOGIN.ERROR,
    error
  });