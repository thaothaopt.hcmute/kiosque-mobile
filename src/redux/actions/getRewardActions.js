import { GET_REWARD } from './actionTypes';

export const requestGetReward = data => ({
  type: GET_REWARD.REQUEST,
  data
});

export const requestGetRewardSuccess = data => ({
  type: GET_REWARD.SUCCESS,
  data
});

export const requestGetRewardError = error => ({
  type: GET_REWARD.ERROR,
  error
});
