import { GET_ADDRESS } from './actionTypes';

export const requestGetAddress = (data) => ({
    type: GET_ADDRESS.REQUEST,
    data
});

export const requestGetAddressSuccess = data => ({
    type: GET_ADDRESS.SUCCESS,
  data
});

export const requestGetAddressError = error => ({
  type: GET_ADDRESS.ERROR,
  error
});
