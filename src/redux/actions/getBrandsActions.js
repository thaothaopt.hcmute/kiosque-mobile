import { GET_BRANDS } from './actionTypes';

export const requestGetBrands = (data) => ({
    type: GET_BRANDS.REQUEST,
    data
});

export const requestGetBrandsSuccess = data => ({
    type: GET_BRANDS.SUCCESS,
  data
});

export const requestGetBrandsError = error => ({
  type: GET_BRANDS.ERROR,
  error
});
