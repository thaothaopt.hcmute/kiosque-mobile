import { GET_NEWS_EVENT } from './actionTypes';

export const requestGetNewsEvent = (data) => ({
  type: GET_NEWS_EVENT.REQUEST,
  data
});

export const requestGetNewsEventSuccess = data => ({
  type: GET_NEWS_EVENT.SUCCESS,
  data
});

export const requestGetNewsEventError = error => ({
  type: GET_NEWS_EVENT.ERROR,
  error
});
