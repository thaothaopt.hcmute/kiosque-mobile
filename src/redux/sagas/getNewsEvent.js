import { takeLatest, call, put } from 'redux-saga/effects';
import { GET_NEWS_EVENT } from '../actions/actionTypes';
import {
  requestGetNewsEventSuccess, requestGetNewsEventError
} from '../actions/getNewsEventActions';
import { requestGetNews } from '../service/api';

export function* requestGetNewsEvent_(action) {
  try {
    const data = yield call(requestGetNews, action.data);
    yield put(requestGetNewsEventSuccess(data.data));
  } catch (error) {
    yield put(requestGetNewsEventError(error));
  }
}

export default function* abc() {
  yield takeLatest(GET_NEWS_EVENT.REQUEST, requestGetNewsEvent_);
}

