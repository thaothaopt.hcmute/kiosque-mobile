import { takeLatest, call, put } from 'redux-saga/effects';
import { GET_USERINFO } from '../actions/actionTypes';
import {
    requestGetUserInfoSuccess, requestGetUserInfoError
} from '../actions/getUserInfoActions';
import { requestGetUserInfo } from '../service/api';

export function* requestGetUserInfo_(action) {
  try {
    const data = yield call(requestGetUserInfo, action.data);
    yield put(requestGetUserInfoSuccess(data.data));
  } catch (error) {
    yield put(requestGetUserInfoError(error));
  }
}

export default function* abc() {
  yield takeLatest(GET_USERINFO.REQUEST, requestGetUserInfo_);
}

