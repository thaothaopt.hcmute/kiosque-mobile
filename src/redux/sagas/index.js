import { fork, all } from 'redux-saga/effects';
import login from './login';
import getType from './getType';
import getNewsDaily from './getNewsDaily';
import getUserInfo from './getUserInfo';
import getNewsEvent from './getNewsEvent';
import getProducts from './getProducts';
import getProductsType from './getProductsType';
import getAddresses from './getAddresses';
import getBrands from './getBrands';
import getProductsBrand from './getProductsBrand';
import getTypesBrand from './getTypesBrand';
import getHistory from './getHistory';
import getReward from './getReward';

export default function* rootSaga() {
    yield all([
        fork(login),
        fork(getType),
        fork(getNewsDaily),
        fork(getUserInfo),
        fork(getNewsEvent),
        fork(getProducts),
        fork(getProductsType),
        fork(getAddresses),
        fork(getBrands),
        fork(getProductsBrand),
        fork(getTypesBrand),
        fork(getHistory),
        fork(getReward)
    ]);
}
