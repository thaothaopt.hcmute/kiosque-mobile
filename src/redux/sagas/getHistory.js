import { takeLatest, call, put } from 'redux-saga/effects';
import { GET_HISTORY } from '../actions/actionTypes';
import {
    requestGetHistorySuccess, requestGetHistoryError
} from '../actions/getHistoryActions';
import { requestGetHistory } from '../service/api';

export function* requestGetHistory_(action) {
  try {
    const data = yield call(requestGetHistory, action.data);
    yield put(requestGetHistorySuccess(data.data));
  } catch (error) {
    yield put(requestGetHistoryError(error));
  }
}

export default function* abc() {
  yield takeLatest(GET_HISTORY.REQUEST, requestGetHistory_);
}

