import { takeLatest, call, put } from 'redux-saga/effects';
import { GET_PRODUCTS_BRAND } from '../actions/actionTypes';
import {
    requestGetProductsBrandSuccess, requestGetProductsBrandError
} from '../actions/getProductsBrandActions';
import { requestGetProductsBrand } from '../service/api';

export function* requestGetProductsBrand_(action) {
  try {
    const data = yield call(requestGetProductsBrand, action.data);
    yield put(requestGetProductsBrandSuccess(data.data));
  } catch (error) {
    yield put(requestGetProductsBrandError(error));
  }
}

export default function* abc() {
  yield takeLatest(GET_PRODUCTS_BRAND.REQUEST, requestGetProductsBrand_);
}

