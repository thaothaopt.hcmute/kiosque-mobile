import { takeLatest, call, put } from 'redux-saga/effects';
import { GET_TYPE } from '../actions/actionTypes';
import {
    requestGetTypeSuccess, requestGetTypeError
} from '../actions/getTypeActions';
import { requestGetType } from '../service/api';

export function* requestGetType_(action) {
  try {
    const data = yield call(requestGetType, action.data);
    yield put(requestGetTypeSuccess(data.data));
  } catch (error) {
    yield put(requestGetTypeError(error));
  }
}

export default function* abc() {
  yield takeLatest(GET_TYPE.REQUEST, requestGetType_);
}

