import { takeLatest, call, put } from 'redux-saga/effects';
import { GET_PRODUCTS_TYPE } from '../actions/actionTypes';
import {
    requestGetProductsTypeSuccess, requestGetProductsTypeError
} from '../actions/getProductsTypeActions';
import { requestGetProductsType } from '../service/api';

export function* requestGetProductsType_(action) {
  try {
    const data = yield call(requestGetProductsType, action.data);
    yield put(requestGetProductsTypeSuccess(data.data));
  } catch (error) {
    yield put(requestGetProductsTypeError(error));
  }
}

export default function* abc() {
  yield takeLatest(GET_PRODUCTS_TYPE.REQUEST, requestGetProductsType_);
}

