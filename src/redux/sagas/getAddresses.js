import { takeLatest, call, put } from 'redux-saga/effects';
import { GET_ADDRESS } from '../actions/actionTypes';
import {
    requestGetAddressSuccess, requestGetAddressError
} from '../actions/getAddressActions';
import { requestGetAddresses } from '../service/api';

export function* requestGetAddresses_(action) {
  try {
    const data = yield call(requestGetAddresses, action.data);
    yield put(requestGetAddressSuccess(data.data));
  } catch (error) {
    yield put(requestGetAddressError(error));
  }
}

export default function* abc() {
  yield takeLatest(GET_ADDRESS.REQUEST, requestGetAddresses_);
}

