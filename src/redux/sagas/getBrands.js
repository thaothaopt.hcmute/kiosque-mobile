import { takeLatest, call, put } from 'redux-saga/effects';
import { GET_BRANDS } from '../actions/actionTypes';
import {
    requestGetBrandsSuccess, requestGetBrandsError
} from '../actions/getBrandsActions';
import { requestGetBrands } from '../service/api';

export function* requestGetBrands_(action) {
  try {
    const data = yield call(requestGetBrands, action.data);
    yield put(requestGetBrandsSuccess(data.data));
  } catch (error) {
    yield put(requestGetBrandsError(error));
  }
}

export default function* abc() {
  yield takeLatest(GET_BRANDS.REQUEST, requestGetBrands_);
}

