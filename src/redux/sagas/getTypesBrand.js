import { takeLatest, call, put } from 'redux-saga/effects';
import { GET_TYPES_BRAND } from '../actions/actionTypes';
import {
    requestGetTypesBrandSuccess, requestGetTypesBrandError
} from '../actions/getTypesBrandActions';
import { requestGetTypesBrand } from '../service/api';

export function* requestGetTypesBrand_(action) {
  try {
    const data = yield call(requestGetTypesBrand, action.data);
    yield put(requestGetTypesBrandSuccess(data.data));
  } catch (error) {
    yield put(requestGetTypesBrandError(error));
  }
}

export default function* abc() {
  yield takeLatest(GET_TYPES_BRAND.REQUEST, requestGetTypesBrand_);
}

