import { takeLatest, call, put } from 'redux-saga/effects';
import { GET_REWARD } from '../actions/actionTypes';
import {
    requestGetRewardSuccess, requestGetRewardError
} from '../actions/getRewardActions';
import { requestGetReward } from '../service/api';

export function* requestGetReward_(action) {
  try {
    const data = yield call(requestGetReward, action.data);
    yield put(requestGetRewardSuccess(data.data));
  } catch (error) {
    yield put(requestGetRewardError(error));
  }
}

export default function* abc() {
  yield takeLatest(GET_REWARD.REQUEST, requestGetReward_);
}

