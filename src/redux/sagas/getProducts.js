import { takeLatest, call, put } from 'redux-saga/effects';
import { GET_PRODUCTS } from '../actions/actionTypes';
import {
    requestGetProductsSuccess, requestGetProductsError
} from '../actions/getProductsActions';
import { requestGetProducts } from '../service/api';

export function* requestGetProducts_(action) {
  try {
    const data = yield call(requestGetProducts, action.data);
    yield put(requestGetProductsSuccess(data.data));
  } catch (error) {
    yield put(requestGetProductsError(error));
  }
}

export default function* abc() {
  yield takeLatest(GET_PRODUCTS.REQUEST, requestGetProducts_);
}

