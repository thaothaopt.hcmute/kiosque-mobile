import { takeLatest, call, put } from 'redux-saga/effects';
import { GET_NEWS_DAILY } from '../actions/actionTypes';
import {
  requestGetNewsDailySuccess, requestGetNewsDailyError
} from '../actions/getNewsDailyActions';
import { requestGetNews } from '../service/api';

export function* requestGetNewsDaily_(action) {
  try {
    const data = yield call(requestGetNews, action.data);
    yield put(requestGetNewsDailySuccess(data.data));
  } catch (error) {
    yield put(requestGetNewsDailyError(error));
  }
}

export default function* abc() {
  yield takeLatest(GET_NEWS_DAILY.REQUEST, requestGetNewsDaily_);
}

