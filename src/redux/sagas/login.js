import { takeLatest, call, put } from 'redux-saga/effects';
import { LOGIN } from '../actions/actionTypes';
import {
    requestLoginSuccess, requestLoginError
} from '../actions/loginActions';
import { requestLogin } from '../service/api';

export function* requestLogin_(action) {
  try {
    const data = yield call(requestLogin, action.data);
    yield put(requestLoginSuccess(data.data));
  } catch (error) {
    yield put(requestLoginError(error));
  }
}

export default function* abc() {
  yield takeLatest(LOGIN.REQUEST, requestLogin_);
}

