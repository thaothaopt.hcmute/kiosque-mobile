import React, { Component } from 'react';
import {
  StyleSheet, TouchableOpacity,
  Text, Image, FlatList,
  View,
  Modal
} from 'react-native';
import { connect } from 'react-redux';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Colors from '../../contants/colors';
import { rootStyles } from '../../contants/styles';
import Screen from '../../utils/screen';
import Strings from '../../contants/strings';
import Button from '../Button';
import TextBox from '../TextBox';
import ItemOptionH from '../ItemOptionH';
import { hardcoderating } from './helper';
/** */

class ModalRating extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataRating: []
    };
  }

  componentDidMount() {
    this.setState({
      dataRating: hardcoderating
    })
  }

  onPressStar = (item) => {
    const { dataRating } = this.state;
  }

  renderItemRating = ({ item }) => {
    return (
      <TouchableOpacity
        onPress={() => this.onPressStar(item)}
        style={{
          width: 30,
          height: 30,
          alignItems: 'center',
          justifyContent: 'center',
          margin: 10
        }}
      >
      <FontAwesome
        name={item.icon}
        size={25}
        color={item.checked ? Colors.STAR_COLOR : Colors.TEXT_COLOR_2}
      />
      </TouchableOpacity>
    );
  }

  render() {
    const { } = this.state;
    const { visible, closeModal, onChangeText, onPressSubmitReview, disabled } = this.props;
    const { dataRating } = this.state;
    return (
      <Modal
        transparent
        visible={visible}
        onRequestClose={() => { }}
      >
        <View style={{ height: '100%', width: '100%', backgroundColor: 'transparent' }}>
          <View style={styles.opacity} />
          <TouchableOpacity
            onPress={closeModal}
            style={styles.closeModal}
          />
          <View style={styles.content}>
            <ItemOptionH
              title={'Rating'}
              style={styles.sectionTitle}
              disabled
            />
            {/* <FlatList
              contentContainerStyle={{
                alignItems: 'center'
              }}
              numColumns={5}
              data={dataRating}
              renderItem={this.renderItemRating}
              keyExtractor={(item, key) => key.toString()}
              extraData={this.state}
            /> */}
            <View style={{ marginHorizontal: Screen.width(4)}}>
            <TextBox
              placeholder={Strings.profile_rating_hint}
              onChangeText={onChangeText}
              selectionColor='#d4cfcf'
              placeholderTextColor={Colors.TEXT_COLOR_2}
              styleTextBox={{color:'#666666'}}
            />
            </View>
            <Button
            disabled={disabled}
              titleBtn={Strings.profile_rating_submit_button}
              onPress={onPressSubmitReview}
              styleBtn={{ alignSelf: 'flex-end', marginHorizontal: Screen.width(4), marginBottom: Screen.width(4) }}
            />
          </View>
          <TouchableOpacity
            onPress={closeModal}
            style={styles.closeModal}
          />
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  content: {
    flexDirection: 'column',
    backgroundColor: Colors.BACKGROUND_COLOR,
    marginHorizontal: Screen.width(4),
    borderRadius: 4
  },
  opacity: {
    backgroundColor: Colors.TEXT_COLOR,
    opacity: 0.5,
    position: 'absolute',
    top: 0,
    right: 0,
    left: 0,
    bottom: 0
  },
  closeModal: {
    width: '100%',
    flex: 0.35
  },
  sectionTitle: {
    color: Colors.TEXT_COLOR,
    fontSize: Screen.width(4),
    marginLeft: 10,
  }
});

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch => ({
});
export default connect(
  mapStateToProps,
  mapDispatchToProps)(ModalRating);