import React, { Component } from 'react';
import {
  StyleSheet,
  Text, TouchableOpacity,
  View
} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Colors from '../contants/colors';
import Screen from '../utils/screen';

class ItemOptionV extends Component {

  render() {
    const { onPress, title, icon, style, disabled, value } = this.props;
    return (
      <TouchableOpacity
        onPress={onPress}
        disabled={disabled}
      >
        <View style={[styles.container, {
          borderColor: Colors.PRIMARY_COLOR,
          borderRadius: 3,
          borderWidth: 1,
          padding: 15,
          justifyContent: 'center'
          }]}>
          {icon &&
            <MaterialIcons
              name={icon}
              size={25}
              color={Colors.PRIMARY_COLOR}
            />
          }
          <Text style={{ color: Colors.TEXT_COLOR, paddingVertical: Screen.width(1), fontSize: Screen.width(4), }}>
            {title}
          </Text>
          {value
        ? (<Text style={{ color: Colors.ERROR_COLOR, fontSize: Screen.width(3.5) }}>
            {value || '_ _ _'}
          </Text>)
    : (<View />)}
          
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flexDirection: 'column',
    paddingHorizontal: 10,
  },
  text: {
    
  }
});
export default ItemOptionV;