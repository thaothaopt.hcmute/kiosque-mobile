import React, { Component } from 'react';
import {
  StyleSheet, TouchableOpacity,
  Text,
  View, TextInput, Image
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';
import Autocomplete from '../../../react-native-autocomplete-input';
import Colors from '../../contants/colors';
import Dash from '../Dash';
import * as Configs from '../../redux/service/config';
import Screen from '../../utils/screen';
import ItemResult from './ItemResult';
import ModalDetailProduct from '../modalDetailProduct';

class SearchEngine extends Component {
  constructor(props) {
    super(props);
    this.state = {
      query: '',
      item: {},
      modalDetailProduct: false,
      products: [],
      noreult: [
        {
          name: 'No result found!',
          price: 'Please type another!',
          productImage: 'https://web.medicbleep.com/images/noSearch.svg'
        }
      ]
    };
  }

  componentWillMount() {
    const { getProducts } = this.props;
    this.setState({ products: getProducts.data });
  }

  setModalDetailProduct = (e, item) => {
    this.setState({
      modalDetailProduct: e,
      item
    });
  }

  onPressClean = () => {
    this.setState({ query: '' });
    this.setModalDetailProduct(false, {});
  }

  findProduct = (query) => {
    if (query === '') {
      return [];
    }
    const { products } = this.state;
    const regex = new RegExp(`${query.trim()}`, 'i');
    return products.filter(product => product.name.search(regex) >= 0);
  }


  render() {
    const { onChangeText, navigation } = this.props;
    const { query, noreult, modalDetailProduct, item } = this.state;
    const products = this.findProduct(query);
    const comp = (a, b) => a.toLowerCase().trim() === b.toLowerCase().trim();
    return (
      <View style={{
        flexDirection: 'row',
        alignItems: 'center', paddingHorizontal: 10,
        borderColor: Colors.TEXT_COLOR_2,
        borderBottomWidth: 1
      }}
      >
        <Autocomplete
          inputContainerStyle={{ backgroundColor: Colors.TRANSPARENT }}
          containerStyle={styles.autocompleteContainer}
          listStyle={{ padding: 10, marginHorizontal: -10, marginBottom: -10 }}
          autoCapitalize="none"
          autoCorrect={false}
          data={(products.length === 1 && comp(query, products[0].name)) ? [] : (products.length === 0 && query) ? noreult : products}
          defaultValue={query}
          onChangeText={text => this.setState({ query: text })}
          placeholder="Enter search"
          renderSeparator={() => <Dash />}
          renderItem={item => {
            return (
              <ItemResult
                name={item.name}
                price={item.price}
                productImage={item.productImage}
                onPressItem={() => this.setState({ query: item.name },
                  () => this.setModalDetailProduct(true, item))}
              />
            );
          }}
        />
        {query
          ? (
            <TouchableOpacity
              style={{ alignSelf: 'flex-start' }}
              onPress={() => this.onPressClean()}
            >
              <Ionicons
                name="ios-close-circle"
                size={25}
                color={Colors.PRIMARY_COLOR}
                style={{ padding: 10 }}
              />
            </TouchableOpacity>
          )
          : (<View />)}
        <ModalDetailProduct
          cleanText={() => this.onPressClean()}
          product={item}
          closeModal={() => this.setModalDetailProduct(false, {})}
          modalVisible={modalDetailProduct}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  autocompleteContainer: {
  },
});

const mapStateToProps = state => ({
  getProducts: state.getProducts
});

const mapDispatchToProps = dispatch => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(SearchEngine);