import React, { Component } from 'react';
import {
  StyleSheet, TouchableOpacity,
  Text, Image,
  View
} from 'react-native';
import * as Configs from '../../redux/service/config';
import Screen from '../../utils/screen';
import Colors from '../../contants/colors';

class ItemResult extends Component {

  render() {
    const { onPressItem, name, price, productImage } = this.props;
    return (
      <TouchableOpacity
      style={{
        padding: 15,
        borderBottomColor: Colors.SECOND_COLOR,
        borderBottomWidth: 1
      }}
      onPress={onPressItem}
    >
      <View style={{ flexDirection: 'row' }}>
        <Image
          style={{ height: 50, width: 50}}
          source={{ uri: productImage }}
        />
        <View style={{ flexDirection: 'column', paddingHorizontal: 15 }}>
          <Text style={{ fontSize: Screen.width(4), color: Colors.TEXT_COLOR }} >{name}</Text>
          <Text style={{ fontSize: Screen.width(3.5), color: Colors.ERROR_COLOR }} >{price}</Text>
        </View>
      </View>
    </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
export default ItemResult;