import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View
} from 'react-native';
import Dash from './Dash';
import Button from './Button';
import Strings from '../contants/strings';
import Colors from '../contants/colors';
import Screen from '../utils/screen';

class BottomSubTotal extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }


  render() {
      const { total, onPress } = this.props;
    return (
        <View style={{ flexDirection: 'column',borderTopColor: Colors.SECOND_COLOR, borderTopWidth: 0.7 }}>
        <View style={styles.totalContainer}
        >
          <Text style={{ fontSize: Screen.width(4.5), color: Colors.TEXT_COLOR }}>
            {`${Strings.TOTAL}: `}
          </Text>
          <Text style={{ fontSize: Screen.width(4.5), color: Colors.ERROR_COLOR }}>
            {`${total} vnd`}
          </Text>
        </View>
        <Dash/>
        <Button
          titleBtn={'Order'}
          onPress={onPress}
          styleBtn={{ marginHorizontal: Screen.width(4), marginVertical: Screen.width(4) }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  totalContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: Screen.width(4),
    justifyContent: 'space-between',
    marginLeft: Screen.width(4),
    marginRight: Screen.width(4)
},
});
export default BottomSubTotal;