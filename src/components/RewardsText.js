import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View
} from 'react-native';
import Strings from '../contants/strings';
import Colors from '../contants/colors';

class RewardsText extends Component {

  render() {
      const { money } = this.props;
    return (
        <Text style={{ fontSize: 12, color: Colors.TEXT_COLOR }}>
        {(money >= 0) ? Strings.YOURREWARDS : Strings.NOREWARDS}
        <Text style={styles.money}>
          {money}
        </Text>
      </Text>
    );
  }
}

const styles = StyleSheet.create({
    money: {
        fontSize: 12,
        fontWeight: 'bold',
        color: Colors.PRIMARY_COLOR
      },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
export default RewardsText;