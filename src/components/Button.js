import React, { Component } from 'react';
import {
    Dimensions,
    StyleSheet,
    Text, TouchableOpacity,
    View
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Colors from '../contants/colors';
import Screen from '../utils/screen';

class Button extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    };


    render() {
        const { icon, titleBtn, onPress, colorText, styleBtn, disabled } = this.props;
        return (
            <TouchableOpacity
            disabled={disabled}
            onPress={onPress}
            style={disabled ? [styles.btnContainer, styleBtn, {
                opacity: 0.7
            }] :[styles.btnContainer, styleBtn]}
            >
                    {icon ?
                        (<Icon
                            name={icon}
                            size={20}
                            color={colorText}
                        />) :
                        (<View />)}

                    <Text style={[styles.buttonText]}>
                        {titleBtn}
                    </Text>
            </TouchableOpacity>

        );
    }
}

const styles = StyleSheet.create({
    btnContainer: {
        backgroundColor: Colors.PRIMARY_COLOR,
        borderRadius: 5,
        paddingHorizontal: Screen.width(4),
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonText: {
        fontSize: Screen.width(4),
        fontFamily: 'Gill Sans',
        textAlign: 'center',
        margin: 10,
        color: Colors.BACKGROUND_COLOR,
    }
});
export default Button;