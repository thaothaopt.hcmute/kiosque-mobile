import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Colors from '../contants/colors';
import Screen from '../utils/screen';

class Share extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }


  render() {
      const { onPress } = this.props;
    return (
        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginHorizontal: 15 }}>
        <TouchableOpacity
          onPress={onPress}
          style={{ width: 25, height: 25, alignItems: 'center', justifyContent: 'center'}}
        >
          <Ionicons
            name="ios-share-alt"
            size={25}
            color="#999999"
          />
        </TouchableOpacity>
        <Text style={{ color: Colors.TEXT_COLOR, fontSize: Screen.width(3.5), paddingHorizontal: 5 }}>
          {'Share'}
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
export default Share;