import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View
} from 'react-native';
import Colors from '../contants/colors';
import Screen from '../utils/screen';

class Empty extends Component {

    render() {
        const { warn } = this.props;
        return (
            <View style={[styles.empty]}>
                {warn
                    ? (
                        <Text style={styles.warn}>
                            {warn}
                        </Text>
                    )
                    : (<View />)
                }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    empty: {
        paddingTop: Screen.width(4),
        width: '100%',
        height: '100%'
    },
    warn: {
        textAlign: 'center',
    fontSize: Screen.width(3.5),
    color: Colors.PRIMARY_COLOR,
    fontWeight: 'bold'
}
});
export default Empty;