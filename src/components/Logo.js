import React, { Component } from 'react';
import {
  StyleSheet,
  View, Image
} from 'react-native';
import Colors from '../contants/colors';

class Logo extends Component {

  render() {
    return (
      <View style={styles.container}>
        <Image
        width = {50}
        height={50}
        //paddingTop={80}
          source={require('../assets/images/K5.png')}
         resizeMode={'contain'}
        borderRadius={100}
        
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    //justifyContent: 'center',
    alignItems: 'center',
   // paddingRight:30,
    marginRight:15
  }
});
export default Logo;