import React, { Component } from 'react';
import {
  ActivityIndicator,
  StyleSheet,
  Text,
  View
} from 'react-native';
import Colors from '../contants/colors';

 class Loading extends Component {

  render() {
    return (
      <View style={styles.container}>
        <ActivityIndicator
          size="small"
          color={Colors.PRIMARY_COLOR}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
export default Loading;