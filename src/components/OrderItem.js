import React, { Component } from 'react';
import {
  StyleSheet,
  Text, TouchableOpacity,
  View
} from 'react-native';
import moment from 'moment';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Colors from '../contants/colors';
import Strings from '../contants/strings';
import { rootStyles } from '../contants/styles';
import Screen from '../utils/screen';

class OrderItem extends Component {
  render() {
      const { productname, _id, total, status, onPressOrder, time } = this.props;
      const name = (productname.length > 1)
      ? `${productname[0]} and ${productname.length - 1 } others`
      : productname;
    return (
        <TouchableOpacity
        style={{ paddingHorizontal: Screen.width(4), paddingVertical: 10 }}
        onPress={onPressOrder}
        >
          <Text style={{ fontSize: Screen.width(4), color: Colors.TEXT_COLOR }}>
            {name}
          </Text>
  
          <Text style={{ fontSize: Screen.width(3.5), color: Colors.TEXT_COLOR_2 }}>
              {`Code: ${_id}`}
          </Text>
  
          <Text style={{ fontSize: Screen.width(3.5), color: Colors.TEXT_COLOR_2 }}>
              {`Total: ${total} vnd`}
          </Text>

          <Text style={{ fontSize: Screen.width(3.5), color: Colors.TEXT_COLOR_2 }}>
              {`Date: ${moment(time).format('LLL')}`}
          </Text>
  
          <View style={rootStyles.row_between}>
            <Text style={{ fontSize: Screen.width(3.5), color: Colors.TEXT_COLOR_2 }}>
              {`${Strings.STATUS}: `}
              {(status === 1) ? Strings.success : (status === -1 || status === -2)
                ? Strings.pending : Strings.failed}
            </Text>
              <AntDesign
                name={(status === 1) ? 'check' : (status === -1 || status === -2)
                  ? 'swap' : 'close'}
                color={(status === 1) ? Colors.PRIMARY_COLOR : (status === -1 || status === -2)
                  ? '#FFCC00' : Colors.ERROR_COLOR}
                size={20}
              />
          </View>
        </TouchableOpacity>
    );
  }
}

export default OrderItem;