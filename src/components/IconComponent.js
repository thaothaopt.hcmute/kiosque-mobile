import React, { Component } from 'react';
import {
  TouchableOpacity,
} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Colors from '../contants/colors';

class IconComponent extends Component {

  render() {
      const { icon, onPress, disabled } = this.props;
    return (
      <TouchableOpacity onPress={onPress} disabled={disabled}>
        <AntDesign
          name={icon}
          size={20}
          color={Colors.PRIMARY_COLOR}
          style={{ padding: 5 }}
        />
      </TouchableOpacity>
    );
  }
}

export default IconComponent;