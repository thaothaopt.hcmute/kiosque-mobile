import React, { Component } from "react";
import { StyleSheet, Text, View, TouchableOpacity, Image } from "react-native";
import AntDesign from "react-native-vector-icons/AntDesign";
import UserPicture from "./UserPicture";
import Colors from "../contants/colors";
import Strings from "../contants/strings";

class Card extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { typename, typeImage, onPressItemType } = this.props;
    return (
      <TouchableOpacity style={[styles.card, { flexDirection: 'column'}]}
        onPress={onPressItemType}
      >
        <Image style={styles.cardImage} source={{uri: typeImage}} resizeMode= 'cover'/>
        <Text style={styles.cardText}>{typename}</Text>
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  card: {
    backgroundColor: "#36b006",
    marginBottom: 10,
    marginLeft: "2%",
    marginRight: "2%",
    width: 150,
    shadowColor: "#ffffff",
    shadowOpacity: 0.2,
    shadowRadius: 1,
    shadowOffset: {
      height: 6,
      width: 3
    },
    height: 150,
    // resizeMode: "cover",
    borderRadius: 5,
    
  },
  cardImage: {
    width: "100%",
    height:"80%",
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    //border:3,
    //borderColor:'white'
  },
  cardText: {
    padding: 5,
    fontSize: 16,
    color: "white",
    textAlign: "center",
    letterSpacing: 3,
    justifyContent: "center"
  }
});
export default Card;
