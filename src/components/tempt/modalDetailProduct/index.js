import React, { Component } from 'react';
import {
  StyleSheet, TouchableOpacity,
  Text, Image,
  View,
  Modal, 
  AppRegistry
} from 'react-native';
import { connect } from "react-redux";
import Button from '../Button';
import Dash from '../Dash';
import AddToCart from './AddToCart';
import Colors from '../../contants/colors';
import { rootStyles } from '../../contants/styles';
import Screen from '../../utils/screen';
import Strings from '../../contants/strings';
import Size from './Size';
import { requestGetProducts } from "../../redux/actions/getProductsActions";
import Container from "../../components/Container";

class ModalDetailProduct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      count: 1,
      isCheckSizeM: true,
      isCheckSizeL: false,
      
    };
  }

  onPressMinus = (e) => {
    this.setState({ count: e - 1});
  }

  onPressPlus = (e) => {
    this.setState({ count: e + 1});
  }

  onPressSizeM = (SizeM, SizeL) => {
    this.setState({
      isCheckSizeM: SizeL,
      isCheckSizeL: SizeM
    })
  }

  onPressSizeL = (SizeM, SizeL) => {
    this.setState({
      isCheckSizeM: SizeL,
      isCheckSizeL: SizeM
    })
  }

  onPressAddtoCart = () => {}

  componentDidMount() {
    const { navigation,dispatchRequestGetProducts } = this.props;
  
    dispatchRequestGetProducts(navigation.state.params.id);
   
  }

componentWillReceiveProps(nextProps) {
    const { getProducts } = nextProps;
    if (getProducts.isSuccess && getProducts.data) {
      this.setState({ dataSource: getProducts.data });
    }
  }
  
  _renderItem = ({ item }) => {
  
    return(
      <View style={{ height: '100%', width: '100%', backgroundColor: 'transparent' }}>
      <View style={styles.opacity} />
      <TouchableOpacity
        onPress={closeModal}
        style={styles.closeModal}
      />
      <View style={styles.content}>
        {/* detail product */}
        <View style={{ flexDirection: 'row' }}>

          <Image
          source={{ uri:item.productImage}}
            resizeMode="cover"
            style={styles.productImage}
          />
          <View style={{ marginVertical: Screen.width(4)}}>
            {/* <Text style={{ fontSize: Screen.width(4), color: Colors.TEXT_COLOR }}>
              {item.name}
            </Text>
            <Text style={{ fontSize: Screen.width(3.5), color: Colors.TEXT_COLOR }}>
              {`${item.price} vnd` }
            </Text> */}
          {/* <Text style={{ fontSize: Screen.width(3), color: Colors.TEXT_COLOR_2 }}>
            {product.description}
          </Text> */}
          </View>
        </View>
        <Dash />
        {/* options */}
        {/* <Size
          onPressSizeM={() => this.onPressSizeM(isCheckSizeM, isCheckSizeL)}
          onPressSizeL={() => this.onPressSizeL(isCheckSizeM, isCheckSizeL)}
          isCheckSizeM
          isCheckSizeL
        /> */}
        <Dash />
        {/* total */}
        <Text style={[styles.optiontitle, { marginHorizontal: Screen.width(4) }]}>
        {`${Strings.TOTAL}:`}
        </Text>
        <View style={[ rootStyles.row_between, { margin: Screen.width(4) }]}>
          <AddToCart
          onPressMinus={() => this.onPressMinus(count)}
          count
          onPressPlus={() => this.onPressPlus(count)}
          />
          {/* <Button
            icon="cart-plus"
            colorText={Colors.BACKGROUND_COLOR}
            onPress={() => this.onPressAddtoCart()}
            titleBtn={` ${(product.price * count) + ((isCheckSizeL)
              ? (10000 * count) : 0)} vnd`}
          /> */}
        </View>
      </View>
      <TouchableOpacity
        onPress={closeModal}
        style={styles.closeModal}
      />
    </View>
  );

};

  render() {
    const { count, isCheckSizeL, isCheckSizeM } = this.state;
    const { closeModal, visible,getProducts } = this.props;
   console.warn('ids:', getProducts.data)
   //console.warn('idf:',this.state.data.product)
    return (
      
      <Modal
        transparent
        visible={visible}
        onRequestClose={() => { }}
      >
     
     <Container
        style={styles.container}
        loading={getProducts.isFetching}

      >
        {getProducts.isSuccess && getProducts.data ? (
          <FlatList
            style={{ paddingTop: "3%", paddingLeft: "3%", paddingRight: "3%" }}
            data={getProducts.data.product}
            keyExtractor={(item, index) => index.toString()}
            renderItem={item => this._renderItem(item)}
            numColumns={1}
            extraData={this.state}
          />
        

        ) : (
            <View />
          )}
      </Container>


        <View style={{ height: '100%', width: '100%', backgroundColor: 'transparent' }}>
          <View style={styles.opacity} />
          <TouchableOpacity
            onPress={closeModal}
            style={styles.closeModal}
          />
          <View style={styles.content}>
            {/* detail product */}
            <View style={{ flexDirection: 'row' }}>
              {/* <Image
              source={{ uri: product.productImage }}
                resizeMode="cover"
                style={styles.productImage}
              /> */}
              {/* <View style={{ marginVertical: Screen.width(4)}}>
                <Text style={{ fontSize: Screen.width(4), color: Colors.TEXT_COLOR }}>
                  {product.name}
                </Text>
                <Text style={{ fontSize: Screen.width(3.5), color: Colors.TEXT_COLOR }}>
                  {`${product.price} vnd` }
                </Text>
              <Text style={{ fontSize: Screen.width(3), color: Colors.TEXT_COLOR_2 }}>
                {product.description}
              </Text>
              </View> */}
            </View>
            <Dash />
            {/* options */}
            {/* <Size
              onPressSizeM={() => this.onPressSizeM(isCheckSizeM, isCheckSizeL)}
              onPressSizeL={() => this.onPressSizeL(isCheckSizeM, isCheckSizeL)}
              isCheckSizeM
              isCheckSizeL
            /> */}
            <Dash />
            {/* total */}
            <Text style={[styles.optiontitle, { marginHorizontal: Screen.width(4) }]}>
            {`${Strings.TOTAL}:`}
            </Text>
            <View style={[ rootStyles.row_between, { margin: Screen.width(4) }]}>
              <AddToCart
              onPressMinus={() => this.onPressMinus(count)}
              count
              onPressPlus={() => this.onPressPlus(count)}
              />
              {/* <Button
                icon="cart-plus"
                colorText={Colors.BACKGROUND_COLOR}
                onPress={() => this.onPressAddtoCart()}
                titleBtn={` ${(product.price * count) + ((isCheckSizeL)
                  ? (10000 * count) : 0)} vnd`}
              /> */}
            </View>
          </View>
          <TouchableOpacity
            onPress={closeModal}
            style={styles.closeModal}
          />
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  content: {
    flexDirection: 'column',
    backgroundColor: Colors.BACKGROUND_COLOR,
    marginHorizontal: Screen.width(4),
    borderRadius: 4
  },
  opacity: {
    backgroundColor: Colors.TEXT_COLOR,
    opacity: 0.5,
    position: 'absolute',
    top: 0,
    right: 0,
    left: 0,
    bottom: 0
  },
  closeModal: {
    width: '100%',
    flex: 0.35
  },
  productImage: {
    width: Screen.width(20),
    height: Screen.width(20),
    margin: Screen.width(4)
  },
  optiontitle: {
    marginTop: Screen.width(4),
    marginRight: Screen.width(4),
    fontSize: Screen.width(3.5),
    color: Colors.TEXT_COLOR
  }
});
const mapStateToProps = state => ({
  getProducts: state.getProducts,
  login: state.login,
  getUserInfo: state.getUserInfo
});

const mapDispatchToProps = dispatch => ({
  dispatchRequestGetProducts: (data) => dispatch(requestGetProducts(data))
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ModalDetailProduct);