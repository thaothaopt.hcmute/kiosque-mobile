import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View
} from 'react-native';
import Strings from '../../contants/strings';
import Colors from '../../contants/colors';
import Screen from '../../utils/screen';

class Size extends Component {

  render() {
    const { onPressSizeM, onPressSizeL, isCheckSizeM, isCheckSizeL } = this.props
    return (
      <View style={{ flexDirection: 'row', marginVertical: Screen.width(3), marginHorizontal: Screen.width(4) }}>
        <Text style={styles.optiontitle}>
          {Strings.SIZE}
        </Text>

        <TouchableOpacity
          onPress={onPressSizeM}
          style={[
            styles.touch,
            {
              backgroundColor: (isCheckSizeL && !isCheckSizeM)
                ? Colors.TRANSPARENT : Colors.PRIMARY_COLOR
            }]}
        >
          <Text style={[
            styles.sizetext,
            {
              color: (isCheckSizeL && !isCheckSizeM)
              ? Colors.TEXT_COLOR_2 : Colors.BACKGROUND_COLOR
            }]}
          >
            M
              </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={onPressSizeL}
          style={[
            styles.touch,
            {
              backgroundColor: (!isCheckSizeL && isCheckSizeM)
                ? Colors.TRANSPARENT : Colors.PRIMARY_COLOR
            }]}
        >
          <Text style={[styles.sizetext, {
            color: (!isCheckSizeL && isCheckSizeM) 
            ? Colors.TEXT_COLOR_2 : Colors.BACKGROUND_COLOR
          }]}
          >
            L
              </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  touch: {
    marginHorizontal: Screen.width(4),
    borderWidth: 0.7,
    borderColor: Colors.PRIMARY_COLOR,
    borderRadius: 3
  },
  sizetext: {
    textAlignVertical: 'center',
    fontSize: Screen.width(4),
    paddingHorizontal: Screen.width(6),
    paddingVertical: Screen.width(2),
  },
  optiontitle: {
    marginTop: Screen.width(4),
    marginRight: Screen.width(4),
    fontSize: Screen.width(3.5),
    color: Colors.TEXT_COLOR
  }
});
export default Size;