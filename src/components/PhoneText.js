import React, { Component } from 'react';
import {
  StyleSheet, TouchableOpacity,
  Text,
  View
} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Colors from '../contants/colors';
import Screen from '../utils/screen';

class PhoneText extends Component {

  render() {
      const { onPressPhone, phone } = this.props;
    return (
      <TouchableOpacity
      style={styles.container}
      onPress={onPressPhone}
      >
      <View style={{ flexDirection: 'column' }}>
      <Text
        style={{ color: Colors.TEXT_COLOR_2 }}
      >
        {'Phone'}
      </Text>
        <Text style={styles.welcome}>
          {phone}
        </Text>
        </View>
        <AntDesign
          name="right"
          size={17}
          color={Colors.PRIMARY_COLOR}
/>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    marginHorizontal: Screen.width(3),
    borderBottomColor: Colors.PRIMARY_COLOR,
    borderBottomWidth: 1
  },
  welcome: {
    color: Colors.PRIMARY_COLOR,
    paddingVertical: 5,
    fontSize: Screen.width(4)
  }
});
export default PhoneText;