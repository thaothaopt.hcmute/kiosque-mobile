import React, { Component } from 'react';
import {
  View, Modal, StyleSheet, ActivityIndicator
} from 'react-native';
import Colors from '../contants/colors';

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  background: {
    position: 'absolute',
    backgroundColor: 'black',
    opacity: 0.4,
    top: 0,
    left: 0,
    right: 0,
    bottom: 0
  }
});


export default class Indicator extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    const { visible } = this.props;
    return (
      <Modal
        visible={visible}
        animationType="none"
        transparent
        onRequestClose={() => console.log('closed modal')}
      >
        <View style={styles.background} />
        <View style={styles.container}>
          <ActivityIndicator
            color={Colors.PRIMARY_COLOR}
            animating
            size="large"
          />
        </View>
      </Modal>
    );
  }
}
