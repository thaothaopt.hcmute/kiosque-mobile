import React, { Component } from 'react';
import {
  StyleSheet,
  Text, TouchableOpacity,
  View
} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Colors from '../contants/colors';
import Screen from '../utils/screen';

class ItemOptionH extends Component {

  render() {
    const { onPress, title, icon, style, disabled, iconRight } = this.props;
    return (
      <TouchableOpacity
        style={styles.container}
        onPress={onPress}
        disabled={disabled}
      >
        <View style={{alignItems: 'center', flexDirection: 'row' }}>
          {icon &&
            <MaterialIcons
              name={icon}
              size={25}
              color={Colors.PRIMARY_COLOR}
            />
          }
          <Text style={[styles.text, style]}>
            {title}
          </Text>
        </View>
        {iconRight
          ? (
            <View style={{ alignSelf: 'flex-end'}}>
            <MaterialIcons
              name={iconRight}
              size={25}
              color={Colors.PRIMARY_COLOR}
              
            />
            </View>
          )
        : (<View />)}
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: 10,
    borderBottomColor: Colors.SECOND_COLOR,
    borderBottomWidth: 0.5,
    justifyContent: 'space-between',
    paddingVertical: Screen.width(2)
  },
  text: {
    color: Colors.TEXT_COLOR,
    fontSize: Screen.width(4),
    marginLeft: 10,
    alignSelf: 'flex-start'
  }
});
export default ItemOptionH;