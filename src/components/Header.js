import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity
} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import UserPicture from './UserPicture';
import Colors from '../contants/colors';
import Strings from '../contants/strings';
import Screen from '../utils/screen';
import RewardsText from './RewardsText';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 15,
    borderBottomColor: Colors.SECOND_COLOR,
    borderBottomWidth: 0.5,
    backgroundColor: Colors.BACKGROUND_COLOR,
    paddingVertical: 10
  },
  username: {
    fontSize: Screen.width(3.5),
    fontWeight: 'bold',
    color: Colors.TEXT_COLOR
  },
  money: {
    fontSize: 12,
    fontWeight: 'bold',
    color: Colors.PRIMARY_COLOR
  },
  touch: {
    width: 40, height: 40,
    justifyContent: 'center',
    position: 'absolute',
    top: 0,
    bottom: 0,
    right: 0
  }
});
class Header extends Component {
  
  render() {
    const { notify, onPressUserPicture, username, money, iconRight, image, userPicture, onPressIconRight, count } = this.props;
    return (
      <View style={styles.container}>
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <UserPicture
            onPressUserPicture={onPressUserPicture}
            style={image}
            userPicture={userPicture}
          />
          <View style={{ flexDirection: 'column', paddingHorizontal: 10 }}>
            <Text style={styles.username}>
              {username}
            </Text>
            <RewardsText 
              money={money}
            />
          </View>
        </View>
        {iconRight
        ? (
            <TouchableOpacity
              style={styles.touch}
              onPress={onPressIconRight}
            >
            {notify && 
            <View style={{
              marginBottom: -10,
              width: 18,
              height: 18,
              backgroundColor: 'red',
              borderRadius: 9,
              justifyContent: 'center',
              alignItems: 'center',
              alignSelf: 'flex-end',
              marginLeft: 3,
              marginRight: 3
            }}>
            <Text style={{
              fontSize: 12,
              color: Colors.BACKGROUND_COLOR,
            }}>
              {(count > 9) ? '9+' : count}
            </Text>
            </View>
            }
            <AntDesign
              name={iconRight}
              size={25}
              color={Colors.PRIMARY_COLOR}
            />
            </TouchableOpacity>
        )
    : (<View/>)}
      </View>
    );
  }
}

export default Header;