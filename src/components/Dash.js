import React, { Component } from 'react';
import {
  View
} from 'react-native';
import Colors from '../contants/colors';

class Dash extends Component {

  render() {
    const { style } = this.props;
    return (
      <View
      style={[{backgroundColor: Colors.SECOND_COLOR, height: 0.7 }, style]}
      />
    );
  }
}

export default Dash;