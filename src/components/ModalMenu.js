// import liraries
import React, { Component } from 'react';
import {
  View, Text, StyleSheet, Modal, TouchableOpacity, ScrollView
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Colors from '../contants/colors';
import Dash from './Dash';
import Screen from '../utils/screen';

// define your styles
const styles = StyleSheet.create({
  container: {
    borderRadius: 10,
    width: '86%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.BACKGROUND_COLOR,
    paddingVertical: Screen.width(4)
  },
  item: {
    height: 45,
    width: '100%',
    justifyContent: 'flex-start',
    alignItems: 'center',
    flexDirection: 'row',

  },
  title: {
    width: '85%',
    fontSize: Screen.width(3.5),
    marginLeft: 7,
    color: Colors.TEXT_COLOR
  },
  icon: { width: '15%', textAlign: 'center' }
});


// create a component
class ModalMenu extends Component {
  render() {
    const {
      menus, onSelected, showMenu, onRequestClose, style,
      headerElement, dashTop = false, dashBottom = false
    } = this.props;
    const renderMenu = [];
    if (menus) {
      menus.forEach((element, index) => {
        renderMenu.push(
          <TouchableOpacity
            key={parseInt(index.toString())}
            onPress={() => onSelected && onSelected(element)}
            style={{ width: '100%' }}
          >
            <View style={styles.item}>
              {element.icon
                ? (
                  <Icon
                    name={element.icon}
                    size={25}
                    color={Colors.PRIMARY_COLOR}
                    style={styles.icon}
                  />
                )
                : <View />}
              <Text style={styles.title}>{element.title}</Text>
              {element.selected
                ? (
                  <Icon
                    name="check"
                    size={25}
                    color={Colors.PRIMARY_COLOR}
                    style={styles.icon}
                  />
                ) : <View />}
            </View>
          </TouchableOpacity>
        );
      });
    }


    return (
      <Modal
        onRequestClose={() => { }}
        animationType="fade"
        visible={showMenu}
        transparent
      >
        <View style={{
          width: '100%',
          justifyContent: 'center',
          alignItems: 'center',
          flex: 1
        }}
        >
          <View
            onTouchStart={onRequestClose}
            style={{
              top: 0,
              left: 0,
              bottom: 0,
              right: 0,
              position: 'absolute',
              backgroundColor: Colors.TEXT_COLOR,
              opacity: 0.5
            }}
          />

          {style ? (
            <View style={[style]}>
              {headerElement || <View />}
              {dashTop ? <Dash style={{ marginTop: 5, marginBottom: 5, }} /> : <View />}
              {menus.length
                ? (
                  <ScrollView
                    keyboardShouldPersistTaps="handled"
                    contentContainerStyle={[styles.container, { width: '100%', borderRadius: 0 }]}
                  >
                    <View style={{}}>
                      {renderMenu}
                    </View>
                  </ScrollView>
                )
                : <View />}
            </View>
          )
            : (
              <View style={[styles.container, style]}>
                {headerElement || <View />}
                {dashBottom ? <Dash style={{ marginTop: 5, marginBottom: 5, }} /> : <View />}
                <View style={{ marginLeft: Screen.width(6) }}>
                  {renderMenu}
                </View>
              </View>
            )}
        </View>
      </Modal>
    );
  }
}
// make this component available to the app
export default ModalMenu;
