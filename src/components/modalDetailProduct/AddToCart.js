import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View
} from 'react-native';
import IconComponent from '../IconComponent';
import Colors from '../../contants/colors';
import Screen from '../../utils/screen';
import { rootStyles } from '../../contants/styles';

class AddToCart extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }


  render() {
    const { onPressMinus, quantity, onPressPlus } = this.props;
    return (
      <View style={[{ flexDirection: 'row' }, rootStyles.center]}>
        <IconComponent
          icon="minuscircleo"
          onPress={onPressMinus}
          disabled={(quantity <= 1) ? true : false}
        />
        <Text style={styles.quantitytext}>
          {quantity}
        </Text>
        <IconComponent
          icon="pluscircle"
          onPress={onPressPlus}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  quantitytext: {
    paddingHorizontal: Screen.width(6),
    paddingVertical: Screen.width(2),
    borderWidth: 0.7,
    borderColor: Colors.PRIMARY_COLOR,
    borderRadius: 3,
    marginHorizontal: 5
  },
});
export default AddToCart;