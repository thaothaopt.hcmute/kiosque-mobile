import React, { Component } from 'react';
import {
  StyleSheet, TouchableOpacity,
  Text, Image,
  View,
  Modal
} from 'react-native';
import { connect } from "react-redux";
import Share from '../Share';
import Favourite from './Favourite';
import Button from '../Button';
import Dash from '../Dash';
import AddToCart from './AddToCart';
import Colors from '../../contants/colors';
import { rootStyles } from '../../contants/styles';
import Screen from '../../utils/screen';
import Strings from '../../contants/strings';
import Size from './Size';
/** */
import Storage from '../../utils/storage';
import { requestUpdateCart } from '../../redux/actions/requestCartActions';
import { AddFavouriteDrink } from '../../redux/service/api';

class ModalDetailProduct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      quantity: 1,
      isCheckSizeM: true,
      isCheckSizeL: false,
      liked: false,
      visible: false,
      clickedAdd: false
    };
  }

  componentWillReceiveProps(nextProps) {
    const { modalVisible, requestCart } = nextProps;
    const { visible, clickedAdd } = this.state;
    if (modalVisible !== visible) {
      this.setModalVisible(modalVisible);
    }

    if (requestCart.selected && clickedAdd) {
      Storage.setData('cart', requestCart.cart);
      this.setState({ clickedAdd: false },
        () => this.setModalVisible(false));
    }
  }

  setModalVisible = (e) => {
    this.setState({ visible: e });
  }

  onPressMinus = (e) => {
    this.setState({ quantity: e - 1 });
  }

  onPressPlus = (e) => {
    this.setState({ quantity: e + 1 });
  }

  onPressSizeM = (SizeM, SizeL) => {
    this.setState({
      isCheckSizeM: SizeL,
      isCheckSizeL: SizeM
    })
  }

  onPressSizeL = (SizeM, SizeL) => {
    this.setState({
      isCheckSizeM: SizeL,
      isCheckSizeL: SizeM
    })
  }

  onPressHeart = (e, product) => {
    const { login } = this.props;
    const favouriteData = {
      authid: login.auth._id,
      productid: product._id,
      name: product.name
    };
    AddFavouriteDrink(favouriteData).then((res) => {
    }).catch((error) => {
    })
    this.setState({ liked: e });
  }

  onPressShare = () => { }

  onPressAddtoCart = (product) => {
    const { dispatchRequestCart, requestCart } = this.props;
    const { isCheckSizeL, quantity } = this.state;
    const total = ((product.price * quantity) + ((isCheckSizeL) ? (10000 * quantity) : 0));
    const dataAsyntoCart = {
      total: total,
      size: isCheckSizeL ? 'L' : 'M',
      quantity: quantity,
      product: product
    };
    this.setModalVisible(false);
    this.setState({ clickedAdd: true },
      () => dispatchRequestCart(dataAsyntoCart))
  }

  render() {
    const { quantity, isCheckSizeL, isCheckSizeM, liked, visible } = this.state;
    const { product, closeModal, requestCart, cleanText, login } = this.props;
    return (
      <Modal
        transparent
        visible={visible}
        onRequestClose={() => { }}
      >
        <View style={{ height: '100%', width: '100%', backgroundColor: 'transparent' }}>
          <View style={styles.opacity} />
          <TouchableOpacity
            onPress={closeModal}
            style={styles.closeModal}
          />
          <View style={styles.content}>
            {/* detail product */}
            <View style={{ flexDirection: 'row' }}>
              <Image
                source={{ uri: product.productImage }}
                resizeMode="cover"
                style={styles.productImage}
              />
              <View style={{ marginVertical: Screen.width(4) }}>
                <Text style={{ fontSize: Screen.width(4), color: Colors.TEXT_COLOR }}>
                  {product.name}
                </Text>
                <Text style={{ fontSize: Screen.width(3.5), color: Colors.TEXT_COLOR }}>
                  {`${product.price} vnd`}
                </Text>
                <Text style={{ fontSize: Screen.width(3), color: Colors.TEXT_COLOR_2 }}>
                  {product.description}
                </Text>
                {/* */}
                <View style={{ flexDirection: 'row' }}>
                  {login.logged
                    ? (
                      <Favourite
                        icon={"heart"}
                        liked={liked}
                        onPress={() => this.onPressHeart(!liked, product)}
                      />
                    )
                    : (<View />)}
                  <Share
                    onPress={() => this.onPressShare()}
                  />
                </View>
              </View>
            </View>
            <Dash />
            {/* options */}
            <Size
              onPressSizeM={() => this.onPressSizeM(isCheckSizeM, isCheckSizeL)}
              onPressSizeL={() => this.onPressSizeL(isCheckSizeM, isCheckSizeL)}
              isCheckSizeM={isCheckSizeM}
              isCheckSizeL={isCheckSizeL}
            />
            <Dash />
            {/* total */}

            <View style={[rootStyles.row_between, { margin: Screen.width(4) }]}>
              <Text style={[styles.optiontitle]}>
                {`${Strings.TOTAL}:`}
              </Text>
              <Button
                icon="cart-plus"
                colorText={Colors.BACKGROUND_COLOR}
                onPress={() => {
                  this.onPressAddtoCart(product),
                    closeModal()
                }}
                titleBtn={` ${(product.price * quantity) + ((isCheckSizeL)
                  ? (10000 * quantity) : 0)} vnd`}
              />
            </View>
          </View>
          <TouchableOpacity
            onPress={closeModal}
            style={styles.closeModal}
          />
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  content: {
    flexDirection: 'column',
    backgroundColor: Colors.BACKGROUND_COLOR,
    marginHorizontal: Screen.width(4),
    borderRadius: 4
  },
  opacity: {
    backgroundColor: Colors.TEXT_COLOR,
    opacity: 0.5,
    position: 'absolute',
    top: 0,
    right: 0,
    left: 0,
    bottom: 0
  },
  closeModal: {
    width: '100%',
    flex: 0.35
  },
  productImage: {
    width: Screen.width(23),
    height: Screen.width(23),
    margin: Screen.width(4)
  },
  optiontitle: {
    marginTop: Screen.width(4),
    marginRight: Screen.width(4),
    fontSize: Screen.width(3.5),
    color: Colors.TEXT_COLOR
  }
});

const mapStateToProps = state => ({
  requestCart: state.requestCart,
  login: state.login
});

const mapDispatchToProps = dispatch => ({
  dispatchRequestCart: (data) => dispatch(requestUpdateCart(data))
});
export default connect(
  mapStateToProps,
  mapDispatchToProps)(ModalDetailProduct);