import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity
} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Colors from '../../contants/colors';
import Strings from '../../contants/strings';
import Screen from '../../utils/screen';

class Favourite extends Component {

  render() {
    const { liked, icon, onPress } = this.props;
    return (
      <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
        <TouchableOpacity
          onPress={onPress}
          style={{ width: 25, height: 25, alignItems: 'center', justifyContent: 'center'}}
        >
          <FontAwesome
            name={icon}
            size={20}
            color={liked ? Colors.HEART_COLOR : '#999999'}
          />
        </TouchableOpacity>
        <Text style={{ color: Colors.TEXT_COLOR, fontSize: Screen.width(3.5), paddingHorizontal: 5 }}>
          {'Like'}
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
export default Favourite;