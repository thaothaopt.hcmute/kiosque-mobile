import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View
} from 'react-native';
import Colors from '../contants/colors';
import Screen from '../utils/screen';
import ItemOptionH from './ItemOptionH';
import Strings from '../contants/strings';
import { rootStyles } from '../contants/styles';

class BlockOrderInfo extends Component {

  render() {
    const { sectionTitle, content, extraContent, extraComponent } = this.props;
    return (
      <View style={{ flexDirection: 'column' }}>
        <ItemOptionH
          title={sectionTitle}
          style={styles.sectionTitle}
          disabled
        />
        {content}
      {extraComponent}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  break: {
    height: 10,
    width: '100%',
    backgroundColor: Colors.SECOND_COLOR
  },
  sectionTitle: {
    color: Colors.TEXT_COLOR,
    fontSize: Screen.width(4),
    marginLeft: 10,
  }
});
export default BlockOrderInfo;