import React, { Component } from "react";
import { StyleSheet, Text, View, TouchableOpacity, Image, Dimensions } from "react-native";
import AntDesign from "react-native-vector-icons/AntDesign";
import UserPicture from "./UserPicture";
import Colors from '../contants/colors';
import Strings from "../contants/strings";
import Screen from '../utils/screen';

class ListCard extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { productName, productImage, productPrice, onPressItemType, onPressItemProduct } = this.props;
    return (
      <TouchableOpacity style={[styles.card, { flexDirection: 'column' }]}
        onPress={onPressItemProduct}
      >
        <View style={{ flexDirection: "row", alignItems: "center" }}>
          <Image style={styles.cardImage} source={{ uri: productImage }} />
          <View style={{ flexDirection: 'column', marginLeft: 10 }}>
            <Text style={styles.cardText}>{productName}</Text>
            <Text style={[styles.cardText, { color: Colors.ERROR_COLOR, fontSize: 18 }]}>{productPrice}</Text>
          </View>
        </View>
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  card: {
    backgroundColor: "white",
    shadowColor: "#000000",
    shadowOpacity: 0.3,
    shadowRadius: 1,
    shadowOffset: {
      height: 6,
      width: 3
    },
    borderRadius: 5,
    width: Screen.width(100) - 20,
    borderColor: Colors.TEXT_COLOR_2,
    borderWidth: 0.5,
    marginVertical: 5
  },
  cardImage: {
    width: Screen.width(23),
    height: Screen.width(23),
  },
  cardText: {
    fontSize: 14,
    color: "black",
    letterSpacing: 3,
    justifyContent: "center"
  }
});
export default ListCard;
