import React, { Component } from 'react';
import {
  StyleSheet, TouchableOpacity,
  Text,
  View
} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Colors from '../contants/colors';
import Screen from '../utils/screen';

class ToolBar extends Component {
  render() {
    const {
      disabled, iconLeft, onPressIconLeft, iconRight, onPressIconRight, title, parentTitle, colorContent
    } = this.props;
    return (
      <View style={styles.container}>
        {iconLeft
          ? (
            <TouchableOpacity
              style={styles.touch}
              onPress={onPressIconLeft}
            >
              <AntDesign
                name={iconLeft}
                size={25}
                color={colorContent || Colors.PRIMARY_COLOR}
              />
            </TouchableOpacity>
          )
          : (null)
        }

        {parentTitle
          ? (
            <Text
              style={{
                marginLeft: 10,
                color: Colors.TEXT_COLOR,
                fontSize: Screen.width(4.5),
                textAlignVertical: 'center',
                alignSelf: 'flex-start'
              }}
              numberOfLines={1}
            >
              {parentTitle}
            </Text>
          )
          : (null)}

        {title
          ? (
            <Text
              style={{
                fontWeight: '600',
                marginHorizontal: Screen.width(3),
                color: colorContent || Colors.TEXT_COLOR,
                fontSize: Screen.width(4.5),
                textAlignVertical: 'center',
                textAlign: 'center',
                alignSelf: 'center'
              }}
              numberOfLines={1}
            >
              {title}
            </Text>
          )
          : (null)}

        {
          iconRight
            ? (
              <TouchableOpacity
                onPress={onPressIconRight}
                style={[styles.touch, { opacity: disabled ? 0.5 : 1, alignSelf: 'flex-end' }]}
                disabled={disabled}
              >
                <FontAwesome
                  name={iconRight}
                  size={25}
                  color={colorContent || Colors.PRIMARY_COLOR}
                />
              </TouchableOpacity>
            )
            : (null)
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    paddingVertical: 5,
    paddingHorizontal: 15,
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: Colors.BACKGROUND_COLOR,
    borderBottomColor: Colors.SECOND_COLOR,
    borderBottomWidth: 0.5,
  },
  touch: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'flex-start'
  }
});
export default ToolBar;
