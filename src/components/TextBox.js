import React, { Component } from 'react';
import {
    TextInput,
    StyleSheet,
    Text,
    View
} from 'react-native';
import Colors from '../contants/colors';
import Icon from 'react-native-vector-icons/FontAwesome';
import Screen from '../utils/screen';

class TextBox extends Component {

    render() {
        const {
            icon, placeholder, onChangeText, styleTextBox, selectionColor, value, placeholderTextColor      
        } = this.props;
        return (
            <View style={styles.container}>
                {icon ?
                    (<Icon
                        name={icon}
                        size={20}
                        color={selectionColor}
                        style={{ flex: 1 }}
                    />) :
                    (<View/>)}
                <TextInput
                    {...this.props}
                    placeholder={placeholder}
                    placeholderTextColor={placeholderTextColor}
                    onChangeText={onChangeText}
                    style={[styles.textbox, styleTextBox]}
                    selectionColor={selectionColor}
                    underlineColorAndroid={selectionColor}
                    value={value}
                    ref={component => this._textInput = component}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.TRANSPARENT,
    flexDirection: 'row',
    height: 50
    },
    textbox: {
        color: Colors.PRIMARY_COLOR,
        width: '100%',
        flex: 9,
        fontSize: Screen.width(4)
    },
});
export default TextBox;