import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View, Image
} from 'react-native';
import Button from './Button';
const src = require('../assets/images/ordernow.jpg');

class EmptyCart extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }


  render() {
    const { onPress } = this.props;
    return (
      <View style={styles.container}>
        <Image
          source={src}
          style={{ width: 200, height: 200*999/978, margin: 15 }}
        />
        <Text style={styles.welcome}>
          {'Your cart is empty'}
        </Text>
        <Button
          titleBtn="Order now"
          onPress={onPress}
          // colorText
          // styleBtn
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
export default EmptyCart;