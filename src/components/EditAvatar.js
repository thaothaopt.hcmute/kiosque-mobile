import React, { Component } from 'react';
import {
  StyleSheet,
  TouchableOpacity, Image,
} from 'react-native';
import ImagePicker from 'react-native-image-crop-picker';
import Colors from '../contants/colors';
import ModalMenu from './ModalMenu';
import * as Configs from '../redux/service/config';

const menu = [
  {
    icon: 'camera',
    title: 'Take photo with camera'
  },
  {
    icon: 'image',
    title: 'Add image from library'
  }
];
class EditAvatar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      uri: '',
      showMenu: false
    };
  }

  handleMenuSelected = (menu) => {
    const { onImagePicked } = this.props;
    this.setState({ showMenu: false }, () => {
      setTimeout(() => {
        if (menu.icon === 'camera') {
          ImagePicker.openCamera({
            width: 300,
            height: 400,
            cropping: true
          }).then((image) => {
            this.setState({ uri: image.path }, () => {
              if (onImagePicked) {
                onImagePicked(image);
              }
            });
          });
        }
        if (menu.icon === 'image') {
          ImagePicker.openPicker({
            width: 300,
            height: 400,
            cropping: true
          }).then((image) => {
            this.setState({ uri: image.path }, () => {
              if (onImagePicked) {
                onImagePicked(image);
              }
            });
          });
        }
      }, 500);
    });
  }

  render() {
      const { userPicture, style, disable } = this.props;
      const img = `${Configs.PICTURE}${userPicture}`;
      const { uri, showMenu } = this.state;

    return (
        <TouchableOpacity
        disabled={disable}
        onPress={() => this.setState({ showMenu: true })}
        >
          <Image
          style={[styles.image, style]}
          source={userPicture ? {uri: uri ? uri : img} : require('../assets/images/user_default.png')}
          />
          <ModalMenu
          onRequestClose={() => this.setState({ showMenu: false })}
          showMenu={showMenu}
          onSelected={this.handleMenuSelected}
          menus={menu}
        />
          </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  image: {
    borderWidth: 0.5,
    borderColor: Colors.PRIMARY_COLOR
  }
});
export default EditAvatar;