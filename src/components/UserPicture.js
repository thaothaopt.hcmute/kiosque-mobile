import React, { Component } from 'react';
import {
  StyleSheet,
  TouchableOpacity, Image,
} from 'react-native';
import Colors from '../contants/colors';
import * as Configs from '../redux/service/config';

class UserPicture extends Component {
  render() {
      const { onPressUserPicture, userPicture, style, disable } = this.props;
      const img = `${Configs.PICTURE}${userPicture}`;

    return (
        <TouchableOpacity
        disabled={disable}
        onPress={onPressUserPicture}
        >
          <Image
          style={[styles.image, style]}
          source={userPicture ? {uri: img} : require('../assets/images/user_default.png')}
          />
          </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  image: {
    borderWidth: 0.5,
    borderColor: Colors.PRIMARY_COLOR
  }
});
export default UserPicture;