import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View, TouchableOpacity
} from 'react-native';
import { connect } from "react-redux";
import AntDesign from 'react-native-vector-icons/AntDesign';
import Screen from '../utils/screen';
import Colors from '../contants/colors';
import Storage from '../utils/storage';

class BottomCart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      count: 0,
    };
  }

  componentWillMount() {
    Storage.getData('cart', (data) => {
      if (data) {
        this.setState({count: this.sumQuantity(data, 0)})
      } else {
      }
    });
  }

  componentWillReceiveProps(nextProps){
    const { requestCart } = nextProps;
    if (requestCart.selected) {
      this.setState({count: this.sumQuantity(requestCart.cart, 0)})
    }
    if (!requestCart.selected && requestCart.cleaned) {
      Storage.removeData('cart');
    }
  }

  sumQuantity = (data, quantity) => {
      data.forEach((element, index) => {
        quantity += element.quantity;
      })
      return quantity;
  }

  onPressCart = (navigation) => {
    navigation.navigate('LastStepOrderScreen');
  }

  render() {
    const { navigation } = this.props;
    const { count } = this.state;
    
    return (
      <TouchableOpacity
        onPress={() => this.onPressCart(navigation)}
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          alignSelf: 'flex-end',
          flexDirection: 'row',
          backgroundColor: Colors.PRIMARY_COLOR,
          width: Screen.width(14),
          height: Screen.width(14),
          borderRadius: Screen.width(7),
          margin: Screen.width(4)
        }}
      >
        
        <AntDesign
          name="shoppingcart"
          size={Screen.width(7)}
          color={Colors.BACKGROUND_COLOR}
        />
        {(count > 0)
          ? (
            <View style={{
              marginTop: -18,
              marginLeft: -5,
              width: 20,
              height: 20,
              backgroundColor: 'red',
              borderRadius: 10,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontSize: 13,
                color: Colors.BACKGROUND_COLOR
              }}
            >
              {count}
            </Text>
            </View>
          )
          : (<View />)}
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  
});

const mapStateToProps = state => ({
  requestCart: state.requestCart
});

const mapDispatchToProps = dispatch => ({
  dispatchRequestCart: (data) => dispatch(requestUpdateCart(data))
});
export default  connect(
  mapStateToProps,
  mapDispatchToProps) (BottomCart);