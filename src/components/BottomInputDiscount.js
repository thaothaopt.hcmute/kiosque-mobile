import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
} from 'react-native';
import TextBox from './TextBox';
import Screen from '../utils/screen';
import Colors from '../contants/colors';
import Strings from '../contants/strings';

class BottomInputDiscount extends Component {
    render() {
        const { onChangeText } = this.props;
        return (
            <View
                style={styles.container}
            >
                
                <Text style={{ fontSize: Screen.width(4.5), color: Colors.TEXT_COLOR }}>
                    {Strings.HAVE_DISCOUNT_CODE}
                </Text>
                <TextBox
                placeholder={Strings.TYPE_DISCOUNT_CODE}
                onChangeText={onChangeText}
                selectionColor={Colors.TEXT_COLOR}
                placeholderTextColor={Colors.TEXT_COLOR_2}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        margin: Screen.width(4)
    },
    totalContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginVertical: Screen.width(4),
        justifyContent: 'space-between'
    },
    break: {
        height: 10,
        width: '100%',
        backgroundColor: Colors.SECOND_COLOR
      }
});
export default BottomInputDiscount;