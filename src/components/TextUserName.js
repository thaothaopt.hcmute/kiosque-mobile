import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View
} from 'react-native';
import Colors from '../contants/colors';
import Screen from '../utils/screen';

 class TextUserName extends Component {

  render() {
      const { style, username } = this.props;
    return (
      <View style={{ paddingHorizontal: Screen.width(3) }}>
        <Text style={[styles.username, style]}>
          {username}
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  username: {
    color: Colors.BLACK,
    fontWeight: 'bold',
    fontSize: Screen.width(4)
  }
});
export default TextUserName;