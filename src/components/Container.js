import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import DialogBox from 'react-native-dialogbox';
import { View, ScrollView, Keyboard } from 'react-native';
import { rootStyles } from '../contants/styles';
import Dialog from '../utils/dialog';
import Indicator from './Indicator';
import Colors from '../contants/colors';

export default class Container extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      stateLoading: false
    };
  }

  componentDidMount = () => {
    const {
      onKeyboardOpen, onKeyboardClose, scrollable,
      autoScrollEnd = true
    } = this.props;
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', () => {
      if (onKeyboardOpen) onKeyboardOpen();
    });
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', () => {
      if (onKeyboardClose) onKeyboardClose();
      Keyboard.dismiss();
      if (scrollable && autoScrollEnd) {
        setTimeout(() => {
          this.getScroller().scrollToEnd({ animated: true });
        }, 100);
      }
    });
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  showQuickMessage = (result) => {
    if (result.hidden) return;
    this.dialog.tip(
      Dialog.createMessage(
        result.title,
        result.message,
        'OK',
        () => { }
      ));
  }

  showMessage = (title, message, button, callback) => {
    this.dialog.tip(
      Dialog.createMessage(
        title,
        message,
        button || 'OK',
        callback
      ));
  }

  showConfirm = (title, message, decline, accept, callback) => {
    this.dialog.confirm(
      Dialog.createConfirms(
        title,
        message,
        decline || 'Cancel',
        accept || 'OK',
        callback
      )
    );
  }

  showLoading = () => {
    this.setState({ stateLoading: true });
  }

  hideLoading = () => {
    this.setState({ stateLoading: false });
  }

  getScroller = () => this.scroller;

  render() {
    const {
      toolbar,
      children,
      scrollable,
      style,
      backgroundColor = Colors.SECONDARY_COLOR,
      containerStyle,
      loading,
      bottomComponent
    } = this.props;
    const { stateLoading } = this.state;

    return (
      <View style={[rootStyles.container, containerStyle]}>
        {toolbar}
        {scrollable
          ? (
            <ScrollView
              ref={scroller => this.scroller = scroller}
              keyboardShouldPersistTaps="handled"
              style={{ flex: 1, width: '100%', backgroundColor: Colors.BACKGROUND_COLOR }}
            >
              <View style={[rootStyles.body, style]}>
                {children}
              </View>
            </ScrollView>
          )
          : (
            <View style={[rootStyles.body, style]}>
              {children}
            </View>
          )
        }
        {bottomComponent}
        <Indicator
          visible={loading || stateLoading}
        />
        <DialogBox
          ref={dialog => this.dialog = dialog}
        />
      </View>
    );
  }
}

Container.propTypes = {
  toolbar: PropTypes.element,
  scrollable: PropTypes.bool,
  children: PropTypes.oneOfType([PropTypes.array, PropTypes.element, PropTypes.object])
};

Container.defaultProps = {
  scrollable: false
};
